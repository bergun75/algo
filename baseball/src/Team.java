import java.util.Arrays;

public class Team
  {
  private final String name;
  private final int order;
  private final int nWins;
  private final int nLoses;
  private final int nRemaining;
  private final int [] nRemainingWithTeams;
  public Team(String name,
              int order,
              int nWins,
              int nLoses,
              int nRemaining,
              int [] nRemainingWithTeams)
    {
    this.name = name;
    this.order = order;
    this.nWins = nWins;
    this.nLoses = nLoses;
    this.nRemaining = nRemaining;
    int size = nRemainingWithTeams.length;
    this.nRemainingWithTeams = Arrays.copyOf(nRemainingWithTeams,
                                             size);
    }
  public String getName()
    {
    return this.name;
    }  
  public int getOrder()
    {
    return this.order;
    }
  public int getNWins()
    {
    return this.nWins;
    }
  public int getNLoses()
    {
    return this.nLoses;
    }
  public int getNRemaining()
    {
    return this.nRemaining;
    }
  public int getNRemainingWithTeam(int teamId)
    {
    return this.nRemainingWithTeams[teamId];
    }
  }

