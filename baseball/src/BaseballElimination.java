import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FordFulkerson;

public class BaseballElimination
  {
  private final Map<String, Integer> teamOrderMap = new HashMap<>();
  private final Map<String, Iterable<String>> eliminationCache 
                                            = new HashMap<>();
  private final Team [] teams;
  private Team currentLeader;
  public BaseballElimination(String filename)
    {
    In scanner = new In(filename);
    int teamSize = scanner.readInt();
    teams = new Team[teamSize];
    readTeamsFromFile(scanner, teamSize);
    }
  private void readTeamsFromFile(In scanner, int teamSize)
    {
    int currentHighestWins = 0;
    for (int i = 0; i < teamSize; i++)
      {
      String teamName = scanner.readString();
      teamOrderMap.put(teamName, i);
      int nWins = scanner.readInt();
      int nLoses = scanner.readInt();
      int nRemaining = scanner.readInt();
      int [] nRemainingWithTeams = new int[teamSize];
      for (int j = 0; j < teamSize; j++)
        {
        nRemainingWithTeams[j] = scanner.readInt();
        }
      teams[i] = new Team(teamName,
                          i,
                          nWins,
                          nLoses,
                          nRemaining,
                          nRemainingWithTeams);
      if (nWins > currentHighestWins)
        {
        currentHighestWins = nWins;
        currentLeader = teams[i];
        }
      }
    }
  public static void main(String...args)
    {
    BaseballElimination division = new BaseballElimination(args[0]);
    for (String team : division.teams()) 
      {
      if (division.isEliminated(team)) 
        {
        StdOut.print(team + " is eliminated by the subset R = { ");
        for (String t : division.certificateOfElimination(team))
            StdOut.print(t + " ");
        StdOut.println("}");
        }
      else 
        {
        StdOut.println(team + " is not eliminated");
        }
      }
    }
  public int numberOfTeams()
    {
    if (null == teams)
      {
      return 0;
      }
    return teams.length; 
    }
  public Iterable<String> teams()
    {
    return teamOrderMap.keySet();
    }
  public int wins(String team)
    {
    return teams[findTeamOrder(team)].getNWins();
    }
  public int losses(String team)
    {
    return teams[findTeamOrder(team)].getNLoses();
    }
  public int remaining(String team)
    {
    return teams[findTeamOrder(team)].getNRemaining();
    }
  public int against(String team1, String team2)
    {
    int index = findTeamOrder(team1);
    return teams[index].getNRemainingWithTeam(findTeamOrder(team2));
    }
  public boolean isEliminated(String team)
    {
    Iterable<String> eliminatedBy = certificateOfElimination(team);
    if (null == eliminatedBy)
      {
      return false;
      }
    return true;
    }
  public Iterable<String> certificateOfElimination(String teamName)
    {
    Team tim = teams[findTeamOrder(teamName)];
    if (eliminationCache.containsKey(teamName))
      {
      return eliminationCache.get(teamName);
      }
    Iterable<String> trivialSet = trivialElimination(tim);
    if (null != trivialSet)
      {
      return trivialSet;
      }
    return nonTrivialElimination(tim);
    }
  private Iterable<String> trivialElimination(Team tim)
    {
    if (null == currentLeader)
      {
      return null;  
      }
    if (tim.getNWins() + tim.getNRemaining() < currentLeader.getNWins())
      {
      eliminationCache.put(
          tim.getName(), 
          new HashSet<String>(Arrays.asList(currentLeader.getName())));
      return eliminationCache.get(tim.getName());
      }
    return null;
    }
  private Iterable<String> nonTrivialElimination(Team tim)
    {
    int n = numberOfTeams();
    int nMatches = (n-1)*(n-2)/2;
    int V = 2 + (n-1) + nMatches;
    int t = V-1;
    int matchesIndex = 1;
    int teamsIndex = 1 + nMatches;
    FlowNetwork fn = new FlowNetwork(V);
    int minusi = 0;
    for (int i = 0; i < n; i++)
      {
      if (i == tim.getOrder())
        {
        minusi--;
        continue;
        }
      int minusj = 0;
      for (int j = i+1; j < n; j++)
        {
        if (j == tim.getOrder())
          {
          minusj--;
          continue;
          }
        fn.addEdge(new FlowEdge(0, 
                                matchesIndex, 
                                teams[i].getNRemainingWithTeam(j)));
        fn.addEdge(new FlowEdge(matchesIndex, 
                                teamsIndex + i + minusi, 
                                Double.POSITIVE_INFINITY));
        fn.addEdge(new FlowEdge(matchesIndex++, 
                                teamsIndex + j + minusi + minusj, 
                                Double.POSITIVE_INFINITY)); 
        }
      fn.addEdge(new FlowEdge(
          teamsIndex + i + minusi, 
          t, 
          tim.getNWins() + tim.getNRemaining() - teams[i].getNWins()));
      }
    FordFulkerson ff = new FordFulkerson(fn, 0, t);
    Set<String> certificateOfElimination = null;
    int plusi = 0;
    for (int i = teamsIndex; i < t; i++)
      {
      int tIndex = i-teamsIndex;
      if (tIndex == tim.getOrder())
        {
        plusi++;
        }
      if (ff.inCut(i))
        {
        if (null == certificateOfElimination)
          {
          certificateOfElimination = new HashSet<>();
          }
        certificateOfElimination.add(teams[tIndex+plusi].getName());
        }
      }
    eliminationCache.put(tim.getName(),
                         certificateOfElimination);
    return certificateOfElimination;
    }
  private int findTeamOrder(String teamName)
    {
    if (!teamOrderMap.containsKey(teamName))
      {
      throw new IllegalArgumentException(
                String.format("no such team %s exists", teamName));
      }
    return teamOrderMap.get(teamName);    
    }
  }
