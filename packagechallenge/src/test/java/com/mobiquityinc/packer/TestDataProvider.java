package com.mobiquityinc.packer;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.testng.annotations.DataProvider;

public class TestDataProvider
  {
	 
	@DataProvider
	public static Object [][] getScannerData()
		{
		PackerScanner scanner = new PackerFileScanner("src/test/resources/input", "UTF-8");
    List<PackageInfo> packageInfoList = scanner.listAllPackageInfo();
    PackageInfo [] packageInfoArr = packageInfoList.toArray(new PackageInfo[0]);
    return new Object[][]{
					 {getSamplePackageInfo1(),
					 packageInfoArr[0]},
					 {getSamplePackageInfo6(),
					 packageInfoArr[1]},
					 {getSamplePackageInfo2(),
					 packageInfoArr[2]},
					 {getSamplePackageInfo3(),
					 packageInfoArr[3]},
					 };
		}
  @DataProvider
  public  static Object [][] getSolverData()
  	{
	return new Object[][]{
							{getSamplePackageInfo1(),
							 15,
							 getSamplePackageResult1(),
							 1},
							 {getSamplePackageInfo2(),
							 15,
							 getSamplePackageResult2(),
							 2},
							 {getSamplePackageInfo3(),
							 15,
							 getSamplePackageResult3(),
							 2},
							 {getSamplePackageInfo4(),
							 15,
							 getSamplePackageResult4(),
							 3},
							 {getSamplePackageInfo4(),
							 2,
							 getSamplePackageResult4a(),
							 1},
							 {getSamplePackageInfo5(),
							 15,
							 getSamplePackageResult5(),
							 3},
							 {getSamplePackageInfo5(),
							 2,
							 getSamplePackageResult5a(),
							 2},
							 {getSamplePackageInfo6(),
							 15,
							 getSamplePackageResult6(),
							 0}};
  	}
  // 81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) 
  // (5,30.18,€9) (6,46.34,€48)
  //
  // 4
  private static PackageInfo getSamplePackageInfo1()
    {
    List<PackageItem> candidateItems = new LinkedList<>();
    candidateItems.add(new PackageItem(1,
                                       53.38,
                                       45));
    candidateItems.add(new PackageItem(2,
                                       88.62,
                                       98));
    candidateItems.add(new PackageItem(3,
                                       78.48,
                                       3));
    candidateItems.add(new PackageItem(4,
                                       72.30,
                                       76));
    candidateItems.add(new PackageItem(5,
                                       30.18,
                                       9));
    candidateItems.add(new PackageItem(6,
                                       46.34,
                                       48));
    PackageInfo testPackageInfo = new PackageInfo(81,
                                                  candidateItems);
    return testPackageInfo;
    }
  private static Set<Integer> getSamplePackageResult1()
  	{
  	Set<Integer> cmpIds = new HashSet<>();
    cmpIds.add(4);
    return cmpIds;
  	}

  // 75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) 
  // (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
  //
  // 2,7
  private static PackageInfo getSamplePackageInfo2()
    {
    List<PackageItem> candidateItems = new LinkedList<>();
    candidateItems.add(new PackageItem(1,
                                       85.31,
                                       29));
    candidateItems.add(new PackageItem(2,
                                       14.55,
                                       74));
    candidateItems.add(new PackageItem(3,
                                       3.98,
                                       16));
    candidateItems.add(new PackageItem(4,
                                       26.24,
                                       55));
    candidateItems.add(new PackageItem(5,
                                       63.69,
                                       52));
    candidateItems.add(new PackageItem(6,
                                       76.25,
                                       75));
    candidateItems.add(new PackageItem(7,
                                       60.02,
                                       74));
    candidateItems.add(new PackageItem(8,
                                       93.18,
                                       35));
    candidateItems.add(new PackageItem(9,
                                       89.95,
                                       78));
    PackageInfo testPackageInfo = new PackageInfo(75,
                                                  candidateItems);
    return testPackageInfo;
    }
  
  private static Set<Integer> getSamplePackageResult2()
		{
		Set<Integer> cmpIds = new HashSet<>();
	  cmpIds.add(2);
	  cmpIds.add(7);
	  return cmpIds;
		}

  // 56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) 
  // (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)  
  // 
  // 8,9
  private static PackageInfo getSamplePackageInfo3()
    {
    List<PackageItem> candidateItems = new LinkedList<>();
    candidateItems.add(new PackageItem(1,
                                       90.72,
                                       13));
    candidateItems.add(new PackageItem(2,
                                       33.80,
                                       40));
    candidateItems.add(new PackageItem(3,
                                       43.15,
                                       10));
    candidateItems.add(new PackageItem(4,
                                       37.97,
                                       16));
    candidateItems.add(new PackageItem(5,
                                       46.81,
                                       36));
    candidateItems.add(new PackageItem(6,
                                       48.77,
                                       79));
    candidateItems.add(new PackageItem(7,
                                       81.80,
                                       45));
    candidateItems.add(new PackageItem(8,
                                       19.36,
                                       79));
    candidateItems.add(new PackageItem(9,
                                       6.76,
                                       64));
    PackageInfo testPackageInfo = new PackageInfo(56,
                                                  candidateItems);
    return testPackageInfo;
    }
  
  private static Set<Integer> getSamplePackageResult3()
		{
		Set<Integer> cmpIds = new HashSet<>();
	  cmpIds.add(8);
	  cmpIds.add(9);
	  return cmpIds;
		}

  // 9 :(1,9.0,€60) (2,2.0,€40) (3,4.0,€10) (4,3.0,€10)
  //
  // 2,3,4
  private static PackageInfo getSamplePackageInfo4()
    {
    List<PackageItem> candidateItems = new LinkedList<>();
    candidateItems.add(new PackageItem(1,
                                       9.00,
                                       60));
    candidateItems.add(new PackageItem(2,
                                       2.00,
                                       40));
    candidateItems.add(new PackageItem(3,
                                       4.00,
                                       10));
    candidateItems.add(new PackageItem(4,
                                       2.99,
                                       10));
    PackageInfo testPackageInfo = new PackageInfo(9,
                                                  candidateItems);
    return testPackageInfo;
    }

  private static Set<Integer> getSamplePackageResult4()
		{
		Set<Integer> cmpIds = new HashSet<>();
	  cmpIds.add(2);
	  cmpIds.add(3);
	  cmpIds.add(4);
	  return cmpIds;
		}
  
  private static Set<Integer> getSamplePackageResult4a()
		{
		Set<Integer> cmpIds = new HashSet<>();
		cmpIds.add(1);
	  return cmpIds;
		}

  
  // 9 :(1,9.0,€60) (2,2.0,€40) (3,4.0,€10) (4,3.0,€20)
  //
  // 2,3,4
  private static PackageInfo getSamplePackageInfo5()
    {
    List<PackageItem> candidateItems = new LinkedList<>();
    candidateItems.add(new PackageItem(1,
                                       9.00,
                                       60));
    candidateItems.add(new PackageItem(2,
                                       2.00,
                                       40));
    candidateItems.add(new PackageItem(3,
                                       4.00,
                                       10));
    candidateItems.add(new PackageItem(4,
                                       2.99,
                                       20));
    PackageInfo testPackageInfo = new PackageInfo(9.00,
                                                  candidateItems);
    return testPackageInfo;
    }
  private static Set<Integer> getSamplePackageResult5()
		{		
	  return getSamplePackageResult4();
		}
  
  private static Set<Integer> getSamplePackageResult5a()
		{		
  	Set<Integer> cmpIds = new HashSet<>();
	  cmpIds.add(2);	  
	  cmpIds.add(4);
	  return cmpIds;
		}

  // 8 :(1,15.3,€34)
  //
  // no fit
  private static PackageInfo getSamplePackageInfo6()
    {
    List<PackageItem> candidateItems = new LinkedList<>();
    candidateItems.add(new PackageItem(1,
                                       15.30,
                                       34));
    PackageInfo testPackageInfo = new PackageInfo(8.00,
                                                  candidateItems);
    return testPackageInfo;
    }
  private static Set<Integer> getSamplePackageResult6()
		{
		Set<Integer> cmpIds = new HashSet<>();	  
	  return cmpIds;
		}
  }
