package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import java.util.List;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.*;

import org.testng.Assert;
import org.testng.annotations.Test;


public class TestPackageItem
  {
  @Test(expectedExceptions = APIException.class)
  public void shouldFailOnHighWeight()
    {
    new PackageItem(1,
                    101.0,
                    10);
    }
  @Test(expectedExceptions = APIException.class)
  public void shouldFailOnInValidWeight()
    {
    new PackageItem(1,
                    -1.0,
                    10);
    }
  @Test(expectedExceptions = APIException.class)
  public void shouldFailOnHighCost()
    {
    new PackageItem(1,
                    100,
                    101);
    }
  @Test(expectedExceptions = APIException.class)
  public void shouldFailOnInValidCost()
    {
    new PackageItem(1,
                    10.0,
                    -1);
    }
 
  }
