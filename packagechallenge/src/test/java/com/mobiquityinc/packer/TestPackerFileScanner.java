package com.mobiquityinc.packer;

import org.testng.Assert;
import org.testng.annotations.Test;


public class TestPackerFileScanner
  {
  @Test(dataProvider = "getScannerData", dataProviderClass = TestDataProvider.class)
  public void shouldScanFile(PackageInfo cmp,
  													 PackageInfo actual)
    {
    
    
      Assert.assertEquals(actual.getWeightCapacity(),
                        cmp.getWeightCapacity());
      for (int j = 0; j < cmp.getCandidateItems().size(); j++)
        {
        Assert.assertEquals(actual.getCandidateItems().get(j).getIndex(),
                            cmp.getCandidateItems().get(j).getIndex());
        Assert.assertEquals(actual.getCandidateItems().get(j).getWeight(),
                            cmp.getCandidateItems().get(j).getWeight());
        Assert.assertEquals(actual.getCandidateItems().get(j).getCost(),
                            cmp.getCandidateItems().get(j).getCost());
        }
    
    }
  }
