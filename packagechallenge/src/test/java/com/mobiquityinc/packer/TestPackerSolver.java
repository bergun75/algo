package com.mobiquityinc.packer;

import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestPackerSolver
  {

  private static final PackerSolverAlgorithm ALGO = PackerSolverAlgorithm.BRUTE_FORCE;
  @Test(dataProvider = "getSolverData", dataProviderClass = TestDataProvider.class)
  public void shouldGetBestFittingItemIds1(PackageInfo testPackageInfo,
		  								   int itemCapacity,
		  								   Set<Integer> cmpIds,
		  								   int expectedSetSize)
    {    
    PackerSolver pSolver = new PackerSolverShell().createSolver(testPackageInfo,
    																														itemCapacity,
                                                                ALGO);
    Set<Integer> ids = pSolver.getBestFittingItemIds();
    Assert.assertEquals(ids.size(),expectedSetSize);    
    Assert.assertTrue(ids.containsAll(cmpIds));
    }
  

  }
