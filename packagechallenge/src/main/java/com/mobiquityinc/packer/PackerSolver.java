package com.mobiquityinc.packer;
  
import java.util.Set;

public interface PackerSolver
  {
  Set<Integer> getBestFittingItemIds();
  }
