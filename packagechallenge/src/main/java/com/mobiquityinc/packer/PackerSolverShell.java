package com.mobiquityinc.packer;
  
import java.util.Set;

/**
 *  The <tt>PackerSolverShell</tt> class is a factory class 
 *  inorder to ease the selection of Algorithm during tests.
 *
 *  @author bergun
 *
 */
public class PackerSolverShell
  {

   public PackerSolver createSolver(PackageInfo packageInfo,
                                             int itemCapacity,
                                             PackerSolverAlgorithm choice)
    {
    PackerSolver solver = null;
    switch(choice)
      {
      case FLOW_NETWORK:
      solver = new PackerSolverFlowNetwork(packageInfo,
                                          itemCapacity);
      break;
      case BRUTE_FORCE:
      default:
      solver = new PackerSolverBruteForce(packageInfo,
                                          itemCapacity);
      break;
      }
    return solver;
    }
  }
