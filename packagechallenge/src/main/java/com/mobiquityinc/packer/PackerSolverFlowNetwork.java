package com.mobiquityinc.packer;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

/**
 *  The <tt>PackerSolverBruteForce</tt> class is trying to solve the problem 
 *  with O(NlogN) time and O(N) memory complexity. 
 *
 *  <p>
 *  This implementation uses Max Binary Heap to process the PackageInfo(s)
 *  in a order that the most valuable items trying to be fit first.
 *  It doesn't yield correct results on every ocasion!!! So its
 *  IMMATURE!
 *
 *  I started drawing the idea on some Flow Network digraphs and
 *  simplified implementing it without a Flow Network graph struct.
 *  If there was a way to sort all the package items so that if we
 *  place them inorder (best fit to worst fit) to the package that would give
 *  us O(NlogN) complexity. But finding such a algorithm was hard because
 *  on each item selection we may need to reorder remaining packages.
 *  And do this for every item again I would end up with
 *  N(N2(logN) = N3logN which maybe worth considering other than O(N!)
 *  Would be continuing to think for a better algorithm...
 *  
 *  I also thought about implementing a flow network for each combination
 *  where each vertice was the package item and was connected to all
 *  other available packages. It was obviously going to end up
 *  O(N!) complexity
 * 
 *  <p>
 *
 *  @author bergun
 *
 */



public class PackerSolverFlowNetwork implements PackerSolver
  {
  // TreeSet in order to keep the ids sorted
  private Set<Integer> bestFittingItemIds = new TreeSet<Integer>();  
  private final int maxItemLimit;

  public PackerSolverFlowNetwork(PackageInfo packageInfo, 
                           int maxItemLimit)
    {
    this.maxItemLimit = maxItemLimit;
    PriorityQueue<PackageItem> maxPQ = new PriorityQueue<>(Collections.reverseOrder());
    for (PackageItem item:packageInfo.getCandidateItems())
      {
      maxPQ.offer(item);
      }
    double capacity = packageInfo.getWeightCapacity();
    while (!maxPQ.isEmpty())
      {
      PackageItem item = maxPQ.poll();
      capacity -= item.getWeight();
      if (capacity < 0.0)
        {
        capacity += item.getWeight();
        continue;
        }
      bestFittingItemIds.add(item.getIndex());
      if (bestFittingItemIds.size() > maxItemLimit) 
          {
          break;
          }
      }
    }
  public Set<Integer> getBestFittingItemIds()
    {
    return bestFittingItemIds;
    }
  }
