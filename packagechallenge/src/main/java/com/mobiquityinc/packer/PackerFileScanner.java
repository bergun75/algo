package com.mobiquityinc.packer;

import java.util.List;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.math.BigDecimal;

import com.mobiquityinc.exception.APIException;


public class PackerFileScanner implements PackerScanner
  {
  private final File file;
  private final String encoding;
  private static final Pattern ITEM_REGEX = Pattern.compile("\\((.*?),(.*?),€(.*?)\\)");
  public PackerFileScanner(String fileName,
                           String encoding)
    {
    this.file = new File(fileName);
    this.encoding = encoding;
    }
  
  public List<PackageInfo> listAllPackageInfo()
    {
    List<PackageInfo> packages = new LinkedList<>();
    try (Scanner sc = new Scanner(file, encoding))
      {
      while (sc.hasNextLine()) {
        processLine(sc.nextLine(), packages);
      }
    } catch (FileNotFoundException e) {
      throw new APIException(e);
    }
    return packages;
    }
  private void processLine (String line, List<PackageInfo> packages)
    {
    String [] tokens = line.split(":");
    double weightCapacity = Integer.parseInt(tokens[0].trim());
    List<PackageItem> items = new LinkedList<>();
    Matcher matcher = ITEM_REGEX.matcher(tokens[1]);
    while (matcher.find())
      {
      int index = Integer.parseInt(matcher.group(1));
      double weight = Double.parseDouble(matcher.group(2));
      double cost = Double.parseDouble(matcher.group(3));
      PackageItem item = new PackageItem(index,
                                         weight,
                                         cost);
      items.add(item);
      
      }
    PackageInfo packageInfo = new PackageInfo(weightCapacity,
                                          items);
    packages.add(packageInfo);
    }
  }
