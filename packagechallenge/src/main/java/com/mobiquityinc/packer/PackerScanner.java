package com.mobiquityinc.packer;

import java.util.List;

public interface PackerScanner
  {
  List<PackageInfo> listAllPackageInfo();
  }
