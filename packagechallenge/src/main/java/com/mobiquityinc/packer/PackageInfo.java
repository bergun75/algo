/**
 * Package details such as maxWeight are hold on this struct
 * All these attributes will contribute in finding appropriate
 * items to put in the package
 * 
 * @author  bergun
 */

package com.mobiquityinc.packer;
import java.util.List;
import java.util.LinkedList;

public class PackageInfo
  {
  // TODO make a struct for Weight with WEIGHT.GRAM WEIGHT.KG
  // like enum for units
  private final double weightCapacity;
  private final List<PackageItem> candidateItems;

  public PackageInfo(double weightCapacity, 
                     List<PackageItem> candidateItems)
    {
    this.weightCapacity = weightCapacity;
    this.candidateItems = new LinkedList<>();
    for ( PackageItem candidateItem: candidateItems)
      {
      // prune out imposible items to fit in the current package
      if (candidateItem.getWeight() <= this.weightCapacity)
        {
        this.candidateItems.add(candidateItem);
        }
      }
    }
  public double getWeightCapacity()
    {
    return weightCapacity;
    }
  public List<PackageItem> getCandidateItems()
    {
    return candidateItems;
    }
   // TODO implement equals and hashcode. 
    
}
