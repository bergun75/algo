package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;

public class PackageItem implements Comparable<PackageItem>
  {
  private final int index;
  // TODO BigDecimal might be considered to be used.
  // TODO make a struct for Weight with WEIGHT.GRAM WEIGHT.KG
  // like enum for units
  private final double weight;
  // TODO make a struct for Cost with MONEY.EURO MONEY.CENTS
  // like enum for units
  private final double cost;

  public PackageItem(int index,
                     double weight,
                     double cost)
    {
    this.index = index;
    if(weight < 0.0 ||
       weight >= 100.00)
      {
      throw new APIException("weight must be > 0.0 and <= 100.0");
      }
    this.weight = weight;
    if(cost < 0.0 ||
       cost >= 100.00)
      {
      throw new APIException("cost must be > 0.0 and <= 100.0");
      }
    this.cost = cost;
    }
  public int getIndex()
    {
    return index;
    }
  public double getWeight()
    {
    return weight;
    }
  public double getCost()
    {
    return cost;
    }
  // TODO I created this part in order to find a better algorithm
  // than the BruteForce one.
  public int compareTo(PackageItem other)
    {

    if ( this.cost > other.cost)
      {
      return 1;
      }
    else if ( this.cost < other.cost)
      {
      return -1;
      }
    else
      {
      if (this.weight < other.weight)
        {
        return 1;
        }
      else if (this.weight > other.weight)
        {
        return -1;
        }
      }
    return 0;
    }
  // TODO must implement equals and hashcode!!!
  }
