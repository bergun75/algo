package com.mobiquityinc.packer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *  The <tt>PackerSolverBruteForce</tt> class is solving the problem 
 *  with O(N!) time and memory complexity. Which is not the best thing
 *  but it gives expected output and can be used for future 
 *  algorithm enhancement verifications. implements PackerSolver
 *  for being able to test with new algorithms on same Unit Tests. 
 *  
 *  if P != NP which is probably most accepted theory fact at the moment than
 *  this problem is intractable and it is NP complete because it is
 *  a ILP problem Integer Linear Programming which reduces down to SAP
 *  NP problem and the proof the problem is ILP:
 *  
 *  Objective function:
 *  a1c1 + a2c2 + ... + ancn
 *  
 *  Constraints
 *  a1w1 + a2w2 + ... + anwn <= package_weight_capacity
 *  a1 + a2 + ... + an <= max_item_count
 *  a1,a2,...,an (are all either 0 or 1)
 *
 *  <p>
 *  This implementation uses recursion for trying every possibility.
 *  <p>
 *
 *  @author bergun
 *
 */

public class PackerSolverBruteForce implements PackerSolver
  {
  // TreeSet in order to keep the ids sorted
  private Set<Integer> bestFittingItemIds = new TreeSet<Integer>();
  // I didnt make this attr a part of PackageInfo in order to be able
  // to use the same PackageInfo for different itemCapacity
  // calculations
  private final int itemCapacity;
  private final double weightCapacity;
  private double curMaxCost = 0.0;
  private double curMaxWeight = 0.0;

  /**
   * Runs Brute Force algorithm with recursion.
   * @param packageInfo Package information such as capacity and items
   * @param itemCapacity number of allowed items in specified packageInfo
   * @throws com.mobiquityinc.exception.APIException on invalid input
   */
  public PackerSolverBruteForce(PackageInfo packageInfo,
                                int itemCapacity)
    {
    this.itemCapacity = itemCapacity;
    this.weightCapacity = packageInfo.getWeightCapacity();
    List<PackageItem> items = packageInfo.getCandidateItems();
    PackageItem [] itemArr = items.toArray(new PackageItem[items.size()]);
    for (int i = 0; i < itemArr.length; i++)
      {      
      Set<Integer> visitedItemsSet = new HashSet<>();
      findBestFittingItems(i,
                           0,
                           0.0,
                           0.0,
                           itemArr,
                           visitedItemsSet);
      }
    }
  
  private void findBestFittingItems(int curItemIndex,
                                    int prevItemCount,
                                    double prevTotalWeight,
                                    double prevTotalCost,
                                    final PackageItem [] itemArr, 
                                    final Set<Integer> visitedItemsSet)
    {
    double curTotalWeight = prevTotalWeight
                            + itemArr[curItemIndex].getWeight();
    double curTotalCost = prevTotalCost
                          + itemArr[curItemIndex].getCost();
    int curItemCount = prevItemCount + 1;
    if (curItemCount > itemCapacity
        || curTotalWeight > weightCapacity)
      {
      updateBestFittingItemIds(prevTotalCost,
                               prevTotalWeight,
                               itemArr,
                               visitedItemsSet);
      return;
      }
    visitedItemsSet.add(curItemIndex);    
    for (int i = 0; i < itemArr.length; i++)
      {
      if (visitedItemsSet.contains(i)) 
        {
        continue;
        }      
      Set<Integer> visitedItemsSetCopy = new HashSet<>(visitedItemsSet);
      findBestFittingItems(i, 
                           curItemCount, 
                           curTotalWeight,
                           curTotalCost,
                           itemArr, 
                           visitedItemsSetCopy);
      }    
    }

  private void updateBestFittingItemIds(double totalCost,
                                        double totalWeight,
                                        final PackageItem [] itemArr,
                                        Set<Integer> visitedItemsSet)
    {
    boolean updateMax = false;
    if (totalCost > curMaxCost)
      {      
      updateMax = true;
      }
    else if (totalCost == curMaxCost)
      {
      if (totalWeight < curMaxWeight)
        {
        updateMax = true;
        }
      }
    if (updateMax)
      {
      curMaxWeight = totalWeight;
      curMaxCost = totalCost;
      bestFittingItemIds.clear();
      for (int itemIndex:visitedItemsSet) 
        {                
          bestFittingItemIds.add(itemArr[itemIndex].getIndex());        
        }
      }
    
    }

 /**
   * Get the results as TreeSet which is sorted.
   */

  public Set<Integer> getBestFittingItemIds()
    {
    return bestFittingItemIds;
    }
  }
