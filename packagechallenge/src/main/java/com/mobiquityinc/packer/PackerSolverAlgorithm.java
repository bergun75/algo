package com.mobiquityinc.packer;

public enum PackerSolverAlgorithm {
    BRUTE_FORCE(0),
    FLOW_NETWORK(1);
    
    private final int id;

    PackerSolverAlgorithm(int id)
      {
      this.id =id;
      }
  }

