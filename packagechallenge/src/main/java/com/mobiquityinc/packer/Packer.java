package com.mobiquityinc.packer;

import java.util.List;
import java.util.Set;

public class Packer
  {
    private final static String FILE_ENCODING = "UTF-8";
    // TODO Dependency Injection because of the assignment requirement
    // see "public static String pack(String file)" I couldn't make
    // the algorithm choice injectable.
    /**
     * SAMPLE INPUT:
     *
     * 81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9)
     *      (6,46.34,€48)
     * 8 : (1,15.3,€34)
     * 75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52)
     * (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
     * 56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36)
     * (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)
     *
     * SAMPLE OUTPUT:
     * 4
     * -
     * 2,7
     * 8,9
     *
     * Parse <tt>file</tt> and return best items to fit in each package in string
     * format.
     * @param file the file for list of packages
     * @throws com.mobiquityinc.APIException when wrong file path or file contents
     * are inappropriate.
     */       
    public static String pack(String file)
      {
      List<PackageInfo> packageInfoList = scanInput(file);
      return solvePackageInfos(packageInfoList);
      }

    private static List<PackageInfo> scanInput(String uri)
      {
      PackerFileScanner scanner = new PackerFileScanner(uri,
                                                        FILE_ENCODING);
      List<PackageInfo> packageInfoList = scanner.listAllPackageInfo();
      return packageInfoList;
      }
    private static String solvePackageInfos(List<PackageInfo> packageInfoList)
      {
      StringBuilder builder = new StringBuilder();
      for (PackageInfo packageInfo: packageInfoList)
        {
        PackerSolver solver = new PackerSolverShell().createSolver(packageInfo,
                                                                   15,
                                                                   PackerSolverAlgorithm.BRUTE_FORCE);
        Set<Integer> ids = solver.getBestFittingItemIds();
        convertSetToString(ids,
                           builder);
        }
      return builder.toString();
      }
    private static void convertSetToString(Set<Integer> ids, 
                                           StringBuilder builder)  
      {
      if (ids.size() == 0)
        {
        builder.append("-\n");
        }
      else
        {
        for (int id:ids)
          {
          builder.append(String.format("%d,", id));
          }
        builder.deleteCharAt(builder.length()-1);
        builder.append("\n");
        }      
      }
       
    /**
     * Unit tests the <tt>Packer</tt> data type.
     * Usage : mvn exec:java -Dexec.mainClass="com.mobiquityinc.packer.Packer" 
     * -Dexec.args="src/main/resources/input"
     */
    public static void main(String ... args)
      {
      System.out.println(pack(args[0]));
      }
  }
