public class InfixToPostfix{
    // eg (5*6)+(7+8)  56*78++ postfix as
    public static void main(String...args){
        // TODO check args
        InfixToPostfix converter = new InfixToPostfix();
        System.out.println(converter.convertToPostfix(args[0]));
    }
    public String convertToPostfix(String infix){
        // TODO check infix format
        Stack<Character> stackOfOperators = new Stack<Character>();
        Stack<Character> stackOfOperands = new Stack<Character>();
        StringBuilder result = new StringBuilder("");
        for(int i =0; i<infix.length();i++){
            // TODO assuming 1 digit numerics only more than 1 digit will fail
            // ignoring operator precedence and expecting parantheses with inFix expression
            char atCursor = infix.charAt(i);
            if(atCursor >= '0' && atCursor <= '9'){
                stackOfOperands.push(atCursor);
            }else if(atCursor == '+' || atCursor == '/' 
                    || atCursor == '-' || atCursor == '*'){
                stackOfOperators.push(atCursor);
            }else if (atCursor == ')'){
                char secondOperand = stackOfOperands.pop();
                if(stackOfOperands.size() > 0){
                    char firstOperand = stackOfOperands.pop();
                    result.append(firstOperand);
                }
                result.append(secondOperand);
                result.append(stackOfOperators.pop());
            }
        }
        while (!stackOfOperators.isEmpty()){
            result.append(stackOfOperators.pop());
        }
      return result.toString();
    }
}
