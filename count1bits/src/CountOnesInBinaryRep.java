public class CountOnesInBinaryRep{

    public static void main (String [] args){
        int arg = Integer.parseInt(args[0]);
        System.out.printf("%s\n",Integer.toBinaryString(arg));
        byte noOfBits = countNumberOfBitsSetToOne(arg);
        System.out.println(noOfBits);
    }

    private static byte countNumberOfBitsSetToOne(int numberInt){
        int mask = 0b0100_0000_0000_0000_0000_0000_0000_0000;
        byte bitsOne = 0;
        while(mask != 0){
            if((mask & numberInt) == mask){
                bitsOne++;
            }
            mask = mask >> 1;
        }
        return bitsOne;
    }
}
