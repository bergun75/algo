import edu.princeton.cs.algs4.In;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayDeque;

public class BoggleSolver
  {
  private final TrieSpellST dict = new TrieSpellST();
  private char [] board;
  private boolean [] isCellVisited;
  private int [] neighbourCellList;
  private Collection<String> words;
  private int callId = 0;
  private int maxRow = 0;
  private int maxCol = 0;
  // Initializes the data structure using the given array of strings 
  // as the dictionary. (You can assume each word in the dictionary 
  // contains only the uppercase letters A through Z.)
  public BoggleSolver(final String[] dictionary)
    {
    for (String word:dictionary)
      {
      if (word.length() > 2)
        {
        dict.put(word, word);
        }      
      }
    }

  // Returns the set of all valid words in the given Boggle board, as an Iterable.
  public Iterable<String> getAllValidWords(final BoggleBoard brd)
    {
    precomputeGraph(brd);
    this.words = new ArrayDeque<String>();
    this.callId++;
    int arraySize = this.maxRow * this.maxCol;
    this.isCellVisited = new boolean[arraySize];
    TrieSpellST.Node node = this.dict.getRootNode();
    if (node == null)
      {
      return this.words;
      }
    for (int cellId = 0; cellId < arraySize; cellId++)
      {
      traverseGraph(cellId,
                    this.dict.getNode(node,
                                      board[cellId]));
      this.isCellVisited[cellId] = false;
      }
    return this.words;
    }

  private void precomputeGraph(final BoggleBoard brd)
    {
    this.maxRow = brd.rows();
    this.maxCol = brd.cols();
    this.board = new char[brd.rows() * brd.cols()];
    this.neighbourCellList = new int[this.maxRow*this.maxCol*8];
    for (int row = 0; row < this.maxRow; row++)
      {
      for (int col = 0; col < this.maxCol; col++)
        {
        int cellId = this.maxCol * row + col;
        board[cellId] = brd.getLetter(row, col);
        int index = 0;
        int cellId8 = cellId << 3;
        int [] neighs = {-1,-1,-1,-1,-1,-1,-1,-1};
        for (int i = row - 1; i <= row + 1; i++)
          {
          if (i < 0 || i >= this.maxRow)
            {
            continue;
            }
          for (int j = col - 1; j <= col + 1; j++)
            {
            if (i == row && j == col)
              {
              continue;
              }
            if (j < 0 ||
                j >= this.maxCol)
              {
              continue;
              }
            else
              {
              int neigCellId = this.maxCol * i + j;
              neighs[index] = neigCellId;
              }
            index++;
            }          
          }
        System.arraycopy(neighs, 0, this.neighbourCellList, cellId8, 8);
        }
      }
    }

  // Returns the score of the given word if it is in the dictionary, zero otherwise.
  // (You can assume the word contains only the uppercase letters A through Z.)
  public int scoreOf(String word)
    {
    if (word.length() < 3)
      {
      return 0;
      }
    if (!dict.contains(word))
      {
      return 0;
      }
    switch(word.length())
      {
      case 3:
      case 4:
        return 1;
      case 5:
        return 2;
      case 6:
        return 3;
      case 7:
        return 5;
      default:
        return 11;
      }
    }
  
  private void traverseGraph(final int cellId,
                             final TrieSpellST.Node node)
    {
    this.isCellVisited [cellId] = true;
    if (null == node)
      {
      return;
      }
    String word = node.getVal(this.callId);
    if (word != null)
      {
      this.words.add(word);
      }
    if (!node.hasAnyChild())
      {
      return;
      }
    for (int i = 7; i >= 0; i--)
      {
      int neigCellId = neighbourCellList[(cellId << 3) + i];
      if (neigCellId == -1)continue;
      if (!this.isCellVisited[neigCellId])
        {
        traverseGraph(neigCellId,
                      this.dict.getNode(node,
                             board[neigCellId]));
        this.isCellVisited [neigCellId] = false;
        }
      }
    }

    public static void main(String...args)
      {
      In scanner = new In(args[0]);
      String [] dict = scanner.readAllStrings();
      BoggleSolver solver = new BoggleSolver(dict);
      BoggleBoard board = new BoggleBoard(args[1]);
      Iterable<String> validWords = null;
      long startT = System.nanoTime();
      validWords = solver.getAllValidWords(board);
      //for (int i = 0; i < 4000; i++)
      //  validWords = solver.getAllValidWords(board);
      long stopT = System.nanoTime();
      int points = 0;
      for (String word:validWords)
        {
        System.out.println(word);
        points += solver.scoreOf(word);
        }
      long duration = (stopT-startT)/1_000_000;
      System.out.printf("Score = %d%n", points);
      System.err.printf("Time = %dms%n", duration);
      }
  }
