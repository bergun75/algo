import edu.princeton.cs.algs4.TST;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.In;
import java.util.Set;
import java.util.HashSet;

public class StringSolver
  {
  private final TST [] tstList = new TST [26*26];
  // Initializes the data structure using the given array of strings 
  // as the dictionary. (You can assume each word in the dictionary 
  // contains only the uppercase letters A through Z.)
  public StringSolver(String[] dictionary)
    {
    int wordCount = 0;
    for (String word:dictionary)
      {
      int index = firstTwoCharToIndex(word);
      if (index == -1)
        {
        continue;
        }
      if (null == tstList[index])
        {
        tstList[index] =  new TST<Integer>();
        }
      if (word.length() > 2)
        {
        tstList[index].put(word.substring(2), wordCount++);
        }
      }
    }
  private int firstTwoCharToIndex(String word)
    {
    if (null == word || word.length() < 2)
      {
      return -1;
      }
    char one = word.charAt(0);
    char two = word.charAt(1);
    return (one - 'A')*26 + (two - 'A');    
  }

  // Returns the set of all valid words in the given String, as an Iterable.
  public Iterable<String> getAllValidWords(String input)
    {
    Set<String> validWords = new HashSet<>();
    for (int i = 0; i < input.length(); i++)
      {
      traverseString(input, i, validWords);
      }
    return validWords;
    }

  private void traverseString(String input, 
                             int index, 
                             Set<String> words)
    {
    Stack<Cell> cellVisitStack = new Stack<>();
    char [] charSequence = getCharSeqToAppend(input.charAt(index));
    boolean [] isCellVisited = new boolean[input.length()];
    cellVisitStack.push(new Cell(new String(charSequence), 
                                 index, 
                                 isCellVisited));
    while (!cellVisitStack.isEmpty())
        {
        Cell cell = cellVisitStack.pop();
        switch (cell.word.length())
          {
          case 1:
          break;
          case 2:
            if (tstList[firstTwoCharToIndex(cell.word)] == null)
              {
              continue;
              }
          break;
          default:
            if (tstList[firstTwoCharToIndex(cell.word)] == null)
              {
              continue;
              }
            if (tstList[firstTwoCharToIndex(cell.word)]
                .get(cell.word.substring(2)) != null)
              {
              words.add(cell.word);
              }
            else if (!tstList[firstTwoCharToIndex(cell.word)].keysWithPrefix(
                cell.word.substring(2)).iterator().hasNext())
              {
              continue;
              }
          break;
          }
          stepNeighbours(input, cell, cellVisitStack);
        }
    }

  private void stepNeighbours(String input, 
                              Cell cell, 
                              Stack<Cell> cellVisitStack)
    {
    for (int i = 0; i < input.length(); i++)
      {
      stepNeighbour(input,
                    cell,
                    cellVisitStack,
                    i);
      }
    }

  private void stepNeighbour(String input,
                             Cell cell,
                             Stack<Cell> cellVisitStack,
                             int index)
  {
  if (!cell.isCellVisited[index])
      {
      char [] appendChar = getCharSeqToAppend(input.charAt(index));
      cellVisitStack.push(new Cell(cell.word.concat(new String(appendChar)),
                          index,
                          cell.isCellVisited));
      }
  }

  private char [] getCharSeqToAppend(char charToAppend)
    {
    char [] array = {charToAppend};
    return array;
    }

  // Returns the score of the given word if it is in the dictionary, zero otherwise.
  // (You can assume the word contains only the uppercase letters A through Z.)
  public int scoreOf(String word)
    {
    if (word.length() < 3)
      {
      return 0;
      }
    else if (word.length() < 5)
      {
      return 1;
      }
    else if (word.length() < 6)
      {
      return 2;
      }
    else if (word.length() < 7)
      {
      return 3;
      }
    else if (word.length() < 8)
      {
      return 5;
      }
    else
      {
      return 11;
      }
    }

    private static class Cell
      {
      private final String word;
      private final boolean [] isCellVisited;
      private int index;
      Cell(String word, int index, boolean [] isCellVisited)
        {
        this.isCellVisited = 
        new boolean[isCellVisited.length];
        for (int i = 0; i < isCellVisited.length; i++)
          {
          this.isCellVisited[i] = isCellVisited[i];
          }
        this.isCellVisited[index] = true;
        this.word = word;
        this.index = index;
        }
      }
    
    public static void main(String...args)
      {
      In scanner = new In(args[0]);
      String [] dict = scanner.readAllStrings();
      StringSolver solver = new StringSolver(dict);
      Iterable<String> validWords = solver.getAllValidWords(args[1]);
      int points = 0;
      for (String word:validWords)
        {
        System.out.println(word);
        points += solver.scoreOf(word);
        }
      System.out.println(points);
      }
  }
