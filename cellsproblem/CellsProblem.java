import java.util.*;
import edu.princeton.cs.algs4.*;

public class CellsProblem{

    private MaxPQ<Path> allPaths = new MaxPQ<Path>(5);
    private byte [][] array;
    public static void main(String...args){
        if(args.length != 2){
            throw new IllegalArgumentException("Please enter numberOf x y coordinates");
        }
        int numberOfX = Integer.parseInt(args[0]);
        int numberOfY = Integer.parseInt(args[1]);
        // start and end I will hardCode can be made arguments later
        int startX = 0;
        int startY = 0;
        int endX = numberOfX-1;
        int endY = numberOfY-1;
        
        CellsProblem prob = new CellsProblem();
        prob.generate2DArrayOfCells(numberOfX, numberOfY);
        prob.display2DArrayOfCells();
        Path shortestPath = prob.returnLeastEffortNeededPath(startX, startY, endX, endY);
        System.out.println();
        for(Cell cell:shortestPath.getListOfCells()){
            System.out.printf("%d,%d,%d\n",cell.getX(), cell.getY(), cell.getEaseOfDiffusion());
        }

    }

    private Path returnLeastEffortNeededPath(int startX, int startY, int endX, int endY){
        Path path = new Path();
        traverseArray(path, startX, startY, endX, endY);
        return allPaths.max();   
    }

    private void traverseArray(Path curPath, int curX, int curY, int endX, int endY){
        Cell curCell = new Cell(curX,curY,array[curX][curY]);
        curPath.addNewCell(curCell);
        if( curX == endX && curY == endY){
            allPaths.insert(curPath);
            for(Cell cell:curPath.getListOfCells()){
                System.out.printf("%d,%d,%d\n",cell.getX(), cell.getY(), cell.getEaseOfDiffusion());
            }
            return;
         }
         Path cur1Path = new Path(curPath);
         Path cur2Path = new Path(curPath);
         Path cur3Path = new Path(curPath);
         if(curY+1 < array[0].length){
             checkAndTraverse(cur1Path, curX,curY+1,endX,endY);
         }
         /*if(curY-1 >= 0){
             contPath= checkAndTraverse(array, curPath, curX,curY-1,endX,endY);
         }*/
         if(curX+1 < array.length){
             checkAndTraverse(cur2Path, curX+1,curY,endX,endY);
         }
         if(curY+1 < array[0].length && curX+1 < array.length){
             checkAndTraverse(cur3Path, curX+1,curY+1,endX,endY);
         }
         /*if(curY-1 >= 0 && curX+1 < array.length){
             contPath= checkAndTraverse(array, curPath, curX+1,curY-1,endX,endY);
         }
         if(curX-1 >= 0){
             contPath= checkAndTraverse(array, curPath, curX-1,curY,endX,endY);
         }
         if(curY+1 < array[0].length && curX-1 >= 0){
             contPath= checkAndTraverse(array, curPath, curX-1,curY+1,endX,endY);
         }
         if(curY-1 >= 0 && curX-1 >= 0){
             contPath= checkAndTraverse(array, curPath, curX-1,curY-1,endX,endY);
         }*/

    }

    private void checkAndTraverse(Path curPath, int curX, int curY, int endX, int endY){
         if(isDiffusableCell(curX,curY)){
            traverseArray(curPath,curX,curY,endX,endY);
         }
    }

    private boolean isDiffusableCell(int nextX, int nextY){
        if(array[nextX][nextY] == 0){
            return false;
        }
        return true;
    }

    // TODO at the moment there is no going back so this is not necessary
    // consider it when going back to a cell is available.
    /*
    private boolean isPreviousCell(Path curPath, int nextX, int nextY){
        if(curPath.getListOfCells().size() > 1){
            int previousCellIndex = curPath.getListOfCells().size() -2;
            Cell cell = curPath.getListOfCells().get(previousCellIndex);
            if(cell.getX() == nextX
                && cell.getY() == nextY){
                return true;
            }
        }
        return false;
    }*/

    private void generate2DArrayOfCells(final int numberOfX, final int numberOfY){
        array = new byte[numberOfX][numberOfY];
        for(int i=0; i<numberOfX;i++){
           for(int j=0; j<numberOfY; j++){
              Random rand = new Random();
              byte weight = (byte) rand.nextInt(100);
              array[i][j] = weight;
           }
        }
    }
    private void display2DArrayOfCells(){
        for(int i=0; i<array.length;i++){
           for(int j=0; j<array[i].length; j++){
              System.out.printf("%4d",array[i][j]);
               if(j == array[i].length-1){
                   System.out.printf("\n");
               }
           }
        }
    }
    
    
}

class Path implements Comparable<Path>{
    private int totalEaseOfDiffusion;
    private List<Cell> listOfCells;

    Path(){
        totalEaseOfDiffusion = 0;
        listOfCells = new LinkedList<>();
    }
    Path(Path path){
        totalEaseOfDiffusion = path.totalEaseOfDiffusion;
        listOfCells = new LinkedList<>(path.listOfCells);
    }

    public void addNewCell(Cell cell){
        totalEaseOfDiffusion += cell.getEaseOfDiffusion();
        listOfCells.add(cell);
    }
    public List<Cell> getListOfCells(){
        return listOfCells;
    }
    public int compareTo(Path other){
       int diff = totalEaseOfDiffusion/listOfCells.size() - other.totalEaseOfDiffusion/other.listOfCells.size();
       return diff;
    }

}

class Cell{ 
    private final int x;
    private final int y;
    private final byte easeOfDiffusion;
    
    Cell(int x, int y, byte ease){
        this.x = x;
        this.y = y;
        this.easeOfDiffusion = ease;
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getEaseOfDiffusion(){
        return easeOfDiffusion;
    }
}
