import java.util.List;
import java.util.ArrayList;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class BurrowsWheeler 
  {
  public static void encode()
  // apply Burrows-Wheeler encoding, reading from standard input 
  // and writing to standard output
    {
    StringBuilder strBuilder = new StringBuilder();
    while (!BinaryStdIn.isEmpty())
      {
      char c = BinaryStdIn.readChar();
      strBuilder.append(c);
      }
      int N = strBuilder.length();
      CircularSuffixArray cfs = new CircularSuffixArray(strBuilder.toString());     
      char [] t = new char[N];
      int first = -1;
      for (int i = 0; i < N; i++)
        {
        if (cfs.index(i) == 0)
          {
          t[i] = strBuilder.charAt(N - 1);
          first = i;
          }
        else
          {
          t[i] = strBuilder.charAt(cfs.index(i) - 1);
          }
        }
      BinaryStdOut.write(first, 32);
      for (int i = 0; i < N; i++)
        {
        BinaryStdOut.write(t[i], 8);
        }
      BinaryStdOut.flush();
    }
  public static void decode()
  // apply Burrows-Wheeler decoding, reading from standard input 
  // and writing to standard output
    {
    int first = BinaryStdIn.readInt();
    char [] tArray = createTArray();
    char [] sortedTArray = tArray.clone();
    LSDChar.sort(sortedTArray);
    int [] nextArray = createNextArray(tArray,
                                       sortedTArray);
    writeOutput(sortedTArray,
                nextArray,
                first);
    }
  private static char[] createTArray()
    {
    StringBuilder tBuilder = new StringBuilder();
    while (!BinaryStdIn.isEmpty())
      {
      tBuilder.append(BinaryStdIn.readChar());
      }
    return tBuilder.toString().toCharArray();
    }
  private static int [] createNextArray(char [] tArray,
                                        char [] sortedTArray)
    {
    List<Queue<Integer>> charIndexList = new ArrayList<>();
    initializeList(charIndexList);
    for (int i = 0; i < tArray.length; i++)
      {
      char c = tArray[i];
      charIndexList.get(c).enqueue(i);
      }
    int [] nextArray = new int[sortedTArray.length];
    for (int i = 0; i < sortedTArray.length; i++)
      {
      nextArray[i] = charIndexList.get(sortedTArray[i]).dequeue();
      }
    return nextArray;
    }

  private static void initializeList(List<Queue<Integer>> charIndexList)
    {
    for (int i = 0; i < Constants.ASCII_R; i++)
      {
      charIndexList.add(new Queue<Integer>());
      }
    }
  private static void writeOutput(char[] sortedTArray,
                                  int [] nextArray,
                                  int first)
    {
    int i = first;
    int N = nextArray.length;
    int count = 0;
    while (count < N)
      {
      BinaryStdOut.write(sortedTArray[i], 8);
      i = nextArray[i];
      count++;
      }
    BinaryStdOut.flush();
    }
  // if args[0] is '-', apply Burrows-Wheeler encoding
  // if args[0] is '+', apply Burrows-Wheeler decoding
  public static void main(String[] args)
    {
    if (args.length != 1)
      {
      throw new IllegalArgumentException("Usage: java BurrowsWheeler <-/+>");
      }
    switch(args[0])
      {
      case "-":
        encode();
      break;
      case "+":
        decode();
      break;
      default:
        throw new IllegalArgumentException("use only '-' or '+'");
      }
    }
  }
  
