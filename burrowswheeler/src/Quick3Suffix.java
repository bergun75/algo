/******************************************************************************
 *  Compilation:  javac Quick3Suffix.java
 *  Execution:    java Quick3Suffix < input.txt
 *  Dependencies: StdIn.java StdOut.java 
 *
 *  Reads string from standard input and 3-way string quicksort them.
 *
 *  % java Quick3Suffix < shell.txt
 *  are
 *  by
 *  sea
 *  seashells
 *  seashells
 *  sells
 *  sells
 *  she
 *  she
 *  shells
 *  shore
 *  surely
 *  the
 *  the
 *
 *
 ******************************************************************************/

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdOut;

/**
 *  The <tt>Quick3Suffix</tt> class provides static methods for sorting an
 *  array of strings using 3-way radix quicksort.
 *  <p>
 *  For additional documentation,
 *  see <a href="http://algs4.cs.princeton.edu/51radix">Section 5.1</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class Quick3Suffix {

    private static String str;
    private static int length;
    private static final int CUTOFF = 15;   // cutoff to insertion sort

    // do not instantiate
    private Quick3Suffix() { } 

    /**  
     * Rearranges the array of strings in ascending order.
     *
     * @param a the array to be sorted
     */
    public static void sort(String stra,
                            int [] a) {
        str = stra;
        length = stra.length();
        StdRandom.shuffle(a);
        sort(a, 0, a.length-1, 0);
    }

    // return the dth character of s, -1 if d = length of s
    private static int charAt(int start,
                              int d) {
        if (d == length)
          {
          return -1;
          }
        int realD = d + start;
        if (realD >= length)
            {
            realD = realD - length;
            }
        return str.charAt(realD);
    }


    // 3-way string quicksort a[lo..hi] starting at dth character
    private static void sort(int[] a, int lo, int hi, int d) { 

        // cutoff to insertion sort for small subarrays
        if (hi <= lo + CUTOFF) {
            insertion(a, lo, hi, d);
            return;
        }
        
        int lt = lo, gt = hi;
        int v = charAt(a[lo], d);
        int i = lo + 1;
        while (i <= gt) {
            int t = charAt(a[i], d);
            if      (t < v) exch(a, lt++, i++);
            else if (t > v) exch(a, i, gt--);
            else              i++;
        }

        // a[lo..lt-1] < v = a[lt..gt] < a[gt+1..hi]. 
        sort(a, lo, lt-1, d);
        if (v >= 0) sort(a, lt, gt, d+1);
        sort(a, gt+1, hi, d);
    }

    // sort from a[lo] to a[hi], starting at the dth character
    private static void insertion(int[] a, 
                                  int lo, 
                                  int hi, 
                                  int d) {
        for (int i = lo; i <= hi; i++)
            {
            for (int j = i; j > lo; j--)
                {
                if (!less(a[j],
                          a[j-1],
                          d))
                  {
                  break;
                  }
                exch(a, j, j-1);
                }
            }
    }

    // exchange a[i] and a[j]
    private static void exch(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    // is v less than w, starting at character d
    // DEPRECATED BECAUSE OF SLOW SUBSTRING EXTRACTION IN JAVA 7
    // private static boolean less(String v, String w, int d) {
    //    assert v.substring(0, d).equals(w.substring(0, d));
    //    return v.substring(d).compareTo(w.substring(d)) < 0; 
    // }

    // is v less than w, starting at character d
    private static boolean less(int v, int w, int d) {
        for (int i = d; i < length; i++) {
            if (charAt(v, i) < charAt(w, i)) return true;
            if (charAt(v, i) > charAt(w, i)) return false;
        }
        return false;
    }

    /**
     * Reads in a sequence of fixed-length strings from standard input;
     * 3-way radix quicksorts them;
     * and prints them to standard output in ascending order.
     */
    public static void main(String[] args) {

        // read in the strings from standard input
        String a = args[0];
        int [] suffixArr = new int[a.length()];
        for (int i = 0; i < a.length(); i++)
          {
          suffixArr[i] = i;
          }
        int N = a.length();

        // sort the strings
        sort(a, suffixArr);

        // print the results
        for (int i = 0; i < N; i++)
          {
            StringBuilder str1 = new StringBuilder(a.substring(suffixArr[i]));
            str1.append(a.substring(0, suffixArr[i]));
            StdOut.printf("%s at index %d%n", 
                           str1, 
                           suffixArr[i]);
          }
    }
}
