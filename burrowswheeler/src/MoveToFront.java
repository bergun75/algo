import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import java.util.Arrays;

public class MoveToFront 
  {
  private static final char [] ASCII_SEQUENCE =  generateASCIISequence();

  // apply move-to-front encoding, reading from 
  // standard input and writing to standard output
  
  public static void encode()
    {
    char [] asciiSeq = Arrays.copyOf(ASCII_SEQUENCE, Constants.ASCII_R);
    try
      {
      while (!BinaryStdIn.isEmpty()) 
        {
        char c = BinaryStdIn.readChar();
        int curIndex = moveToFront(asciiSeq, c);
        BinaryStdOut.write(curIndex, Constants.BITS_PER_CHAR);
        }
      }
    finally
      {
      BinaryStdOut.flush();
      }
    }
  // apply move-to-front decoding, reading from 
  // standard input and writing to standard output
  public static void decode()
    {
    char [] asciiSeq = Arrays.copyOf(ASCII_SEQUENCE, Constants.ASCII_R);
    try
      {
      while (!BinaryStdIn.isEmpty()) 
        {
        int val = BinaryStdIn.readInt(Constants.BITS_PER_CHAR);
        char curChar = moveBackOriginal(asciiSeq, val);
        BinaryStdOut.write(curChar, Constants.BITS_PER_CHAR);
        }
      }
    finally
      {
      BinaryStdOut.flush();
      }
    }

  private static int moveToFront(char [] asciiSeq, char c)
    {
    int curIndex = findIndex(asciiSeq,
                             c);
    arrayCopy(asciiSeq,
              curIndex,
              c);
    return curIndex;
    }

  private static char moveBackOriginal(char [] asciiSeq, int c)
    {
    char curChar = asciiSeq[c];
    arrayCopy(asciiSeq,
              c,
              curChar);
    return curChar;
    }
  
  private static int findIndex(char [] asciiSeq,
                               char c)
    {
    int curIndex = -1;
    for (int i = 0; i < Constants.ASCII_R; i++)
      {
      if (asciiSeq[i] == c)
        {       
        curIndex = i;
        break; 
        }
      }
    // assert curIndex != -1 ?
    return curIndex;
    }

  private static void arrayCopy(char [] asciiSeq,
                                int curIndex,
                                char curChar)
    {
    if (curIndex != 0)
      {
      System.arraycopy(asciiSeq,
                       0,
                       asciiSeq,
                       1,
                       curIndex);
      asciiSeq[0] = curChar;
      }
    }

  private static char [] generateASCIISequence()
    {
    char [] asciiSeq = new char[Constants.ASCII_R];
    for (int i = 0; i < Constants.ASCII_R; i++)
      {
      asciiSeq[i] = (char) i;
      }
    return asciiSeq;
    }

  // if args[0] is '-', apply move-to-front encoding
  // if args[0] is '+', apply move-to-front decoding
  public static void main(String[] args)
    {
    if (args.length != 1)
      {
      throw new IllegalArgumentException("Usage: java MoveToFront <-/+>");
      }
    switch(args[0])
      {
      case "-":        
        encode();
      break;
      case "+":
        decode();
      break;
      default:
        throw new IllegalArgumentException("use only '-' or '+'");
      }
    }
  }
