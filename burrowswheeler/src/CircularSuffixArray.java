import edu.princeton.cs.algs4.StdIn;

public class CircularSuffixArray 
  {
    private final int [] suffixArray;
    public CircularSuffixArray(String s) 
    // circular suffix array of s
      {
      if (null == s)
        {
        throw new NullPointerException("s cannot be null or empty");
        }
      suffixArray = new int[s.length()];
      for (int i = 0; i < s.length(); i++)
        {
        suffixArray[i] = i;
        }
      Quick3Suffix.sort(s, 
                        suffixArray);
      }
    public int length()
    // length of s
      {
      return suffixArray.length;
      }
    public int index(int i)
    // returns index of ith sorted suffix
      {
      if (i < 0 || i >= length())
        {
        throw new IndexOutOfBoundsException(String.format("%d is out of range 0-%d",
                                                           i,
                                                           length()));
        }
      return suffixArray[i];
      }
    public static void main(String[] args) 
    // unit testing of the methods (optional)
      {
      String s = StdIn.readString();
      System.out.println("constructor starts");
      long start = System.nanoTime();
      CircularSuffixArray csa = new CircularSuffixArray(s);
      long end = System.nanoTime();
      System.out.printf("%dms%n", (end-start) / 1000_000);
      for (int i = 0; i < s.length(); i++)
        {
        System.out.printf("%d %s%s%n",
                          csa.index(i),
                          s.substring(csa.index(i)),
                          s.substring(0, csa.index(i)));
        }
      }
  }
