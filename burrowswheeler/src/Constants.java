
public class Constants
{
  public static final int ASCII_R = 256;

  public static final int BITS_PER_CHAR = (int) (Math.log(ASCII_R) / Math.log(2));
  // number of bits needed for each alphabet char.

}
