import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.StdDraw;

/**
 *
 * @author bergun
 */

public class PointSET 
  {
  // construct an empty set of points 
  private final SET<Point2D> setOfPoints;

  public PointSET() 
    {
    setOfPoints = new SET<>();
    }

  // is the set empty? 
  public boolean isEmpty() 
    {
    return size() == 0;
    }

  // number of points in the set 
  public int size() 
    {
    return setOfPoints.size();
    }

  // add the point to the set (if it is not already in the set)
  public void insert(Point2D p) 
    {
    if (null == p) 
      {
      throw new NullPointerException();
      }
    setOfPoints.add(p);
    }

  // does the set contain point p? 
  public boolean contains(Point2D p) 
    {
    if (null == p) 
      {
      throw new NullPointerException();
      }
    return setOfPoints.contains(p);
    }

  // draw all points to standard draw 
  public void draw() 
    {
    for (Point2D point : setOfPoints) 
      {
      StdDraw.point(point.x(), point.y());
      }
    }

  // all points that are inside the rectangle 
  public Iterable<Point2D> range(RectHV rect) 
    {
    if (null == rect) 
      {
      throw new NullPointerException();
      }
    SET<Point2D> pointsInsideRect = new SET<>();
    for (Point2D p : setOfPoints) 
      {
      if (rect.contains(p)) 
        {
        pointsInsideRect.add(p);
        }
      }
    return pointsInsideRect;
    }

  // a nearest neighbor in the set to point p; null if the set is empty 
  public Point2D nearest(Point2D p) 
    {
    if (null == p) 
      {
      throw new NullPointerException();
      }
    if (isEmpty()) 
      {
      return null;
      }
    double smallestDist = Double.POSITIVE_INFINITY;
    Point2D result = null;
    for (Point2D point : setOfPoints) 
      {
      double distance = p.distanceTo(point);
      if (distance <= smallestDist) 
        {
        smallestDist = distance;
        result = point;
        }
      }
    return result;
    }

  // unit testing of the methods (optional) 
  public static void main(String[] args) 
    {
      if (args.length != 1) 
        {
        throw new IllegalArgumentException("provide filename as argument");
        }
      PointSET pointSet = new PointSET();
      pointSet.readPointsFromFile(args[0]);
      Stopwatch stopwatch = new Stopwatch();
      Point2D point = new Point2D(0.534766, 0.845211);
      Point2D closestPoint = pointSet.nearest(point);
      System.out.println(closestPoint);
      double seconds = stopwatch.elapsedTime();
      System.out.printf("size:%-25d seconds: %10.5f result: %s\n",
              pointSet.size(), seconds, closestPoint);

      RectHV rectangle = new RectHV(0.54, 0.54, 0.57, 0.57);
      Stopwatch stopwatch2 = new Stopwatch();
      Iterable<Point2D> withinRectangle = pointSet.range(rectangle);
      double seconds2 = stopwatch2.elapsedTime();
      int count = 0;
      for (Point2D point2 : withinRectangle) 
        {
        count++;
        }
      System.out.printf("size:%-25d seconds: %10.5f number of points: %d\n",
              pointSet.size(), seconds2, count);
    }

  private void readPointsFromFile(String fileName) 
    {
    In scanner = new In(fileName);
    double[] coords = scanner.readAllDoubles();
    for (int i = 0; i < coords.length; i += 2) 
      {
      Point2D point = new Point2D(coords[i], coords[i + 1]);
      setOfPoints.add(point);
      }
    }
}
