import edu.princeton.cs.algs4.Stopwatch;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.Queue;

/**
 *
 * @author bergun
 */
public class KdTree 
  {
  private static final boolean VERTICAL = true;
  private static final boolean HORIZANTAL = false;

  private class Node 
    {
    private final double key;           // key
    private final Point2D val;         // associated data
    private Node left, right;  // links to left and right subtrees
    private final boolean isKeyVertical;     // direction of parent link
    private int N;             // subtree count
    private final RectHV nodesGrid;

    public Node(double key, 
                Point2D val, 
                RectHV nodesGrid,
                boolean isKeyVertical,
                int N) 
      {
      this.key = key;
      this.val = val;
      this.isKeyVertical = isKeyVertical;
      this.N = N;
      this.nodesGrid = nodesGrid;
      }

    /**
     * ***********************************************************************
     * Node helper methods
     * ***********************************************************************
     */
    // is node x red; false if x is null ?
    private boolean isVertical() 
      {
      return this.isKeyVertical;
      }
  }

  private Node root;

  public KdTree() 
    {
    }

  // is the set empty? 
  public boolean isEmpty() 
    {
    return size() == 0;
    }

  // number of points in the set 
  public int size() 
    {
    if (null == root) 
      {
      return 0;
      }
    return root.N;
    }

  // add the point to the set (if it is not already in the set)
  public void insert(Point2D p) 
    {
    if (null == p) 
      {
      throw new NullPointerException();
      }
    if (contains(p)) 
      {
      return;
      }
    root = insert(root, p, null, VERTICAL);
    }

  private Node insert(Node current, 
                      Point2D p,
                      RectHV nodesGrid, 
                      boolean isVertical) 
    {
    if (null == current) 
      {
      RectHV rectGrid = nodesGrid;
      if (null == rectGrid) 
        {
        rectGrid = new RectHV(0.0, 0.0, 1.0, 1.0);
        }
      double key;
      if (isVertical) 
        {
        key = p.x();
        } 
      else 
        {
        key = p.y();
        }
      return new Node(key, p, rectGrid, isVertical, 1);
      }
    current.N++;
    double pointCoordPartToCmp;
    if (current.isKeyVertical == VERTICAL) 
      {
      pointCoordPartToCmp = p.x();
      } 
    else 
      {
      pointCoordPartToCmp = p.y();
      }

    boolean nextDirection = current.isKeyVertical;
    if (nextDirection == VERTICAL) 
      {
      nextDirection = HORIZANTAL;
      } 
    else 
      {
      nextDirection = VERTICAL;
      }
    if (current.key > pointCoordPartToCmp) 
      {
      RectHV leftGrid;
      if (null != current.left) 
        {
        leftGrid = current.left.nodesGrid;
        } 
      else 
        {
        if (current.isKeyVertical == VERTICAL) 
          {
            leftGrid = new RectHV(current.nodesGrid.xmin(),
                                  current.nodesGrid.ymin(),
                                  current.val.x(), 
                                  current.nodesGrid.ymax());
          } 
        else 
          {
            leftGrid = new RectHV(current.nodesGrid.xmin(),
                                  current.nodesGrid.ymin(),
                                  current.nodesGrid.xmax(), 
                                  current.val.y());
          }
        }
      current.left = insert(current.left, p, leftGrid, nextDirection);
      } 
    else 
      {
      RectHV rightGrid;
      if (null != current.right) 
        {
        rightGrid = current.right.nodesGrid;
        } 
      else 
        {
        if (current.isKeyVertical == VERTICAL) 
          {
          rightGrid = new RectHV(current.val.x(),
                                 current.nodesGrid.ymin(),
                                 current.nodesGrid.xmax(), 
                                 current.nodesGrid.ymax());
          } 
        else 
          {
          rightGrid = new RectHV(current.nodesGrid.xmin(),
                                 current.val.y(),
                                 current.nodesGrid.xmax(), 
                                 current.nodesGrid.ymax());
          }
        }
      current.right = insert(current.right, p, rightGrid, nextDirection);
      }
    return current;
    }

  // does the set contain point p? 
  public boolean contains(Point2D p) 
    {
    if (null == p) 
      {
      throw new NullPointerException();
      }
    return null != get(p);
    }

  // value associated with the given key; null if no such key
  private Point2D get(Point2D p) 
    {
    return get(root, p);
    }

  // value associated with the given key in subtree rooted at x;
  // null if no such key
  private Point2D get(Node x, Point2D p) 
    {
    Node current = x;
    while (current != null) 
      {
      if (p.equals(current.val)) 
        {
        return current.val;
        } 
      else 
        {
        double keyToCmp;
        if (current.isKeyVertical == VERTICAL) 
          {
          keyToCmp = p.x();
          } 
        else 
          {
          keyToCmp = p.y();
          }
        if (current.key > keyToCmp) 
          {
          current = current.left;
          } 
        else 
          {
          current = current.right;
          }
        }
    }
    return null;
    }

  // draw all points to standard draw 
  public void draw() 
    {
    for (Node x : nodes()) 
      {
      StdDraw.setPenColor(255, 255, 255);
      x.val.draw();
      if (x.isKeyVertical) 
        {
        StdDraw.setPenColor(0, 0, 255);
        } 
      else 
        {
        StdDraw.setPenColor(255, 0, 0);
        }
      x.nodesGrid.draw();
      }
    }

  private Iterable<Node> nodes() 
    {
    Queue<Node> queue = new Queue<Node>();
    nodes(root, queue);
    return queue;
    }

  private void nodes(Node x, 
                      Queue<Node> queue) 
    {
    if (x == null) 
      {
      return;
      }
    queue.enqueue(x);
    nodes(x.left, queue);
    nodes(x.right, queue);
    }

  // all points that are inside the rectangle 
  public Iterable<Point2D> range(RectHV rect) 
    {
    if (null == rect) 
      {
      throw new NullPointerException();
      }
    Queue<Point2D> pointsInsideRect = new Queue<>();
    if (isEmpty()) 
      {
      return pointsInsideRect;
      }
    range(rect, root, pointsInsideRect);
    return pointsInsideRect;
    }

  private void range(RectHV rect, 
                     Node current,
                     Queue<Point2D> pointsInsideRect) 
    {
    if (null == current) 
      {
      return;
      }
    if (null != current.left && rect.intersects(current.left.nodesGrid)) 
      {
      range(rect, current.left, pointsInsideRect);
      }
    if (null != current.right && rect.intersects(current.right.nodesGrid)) 
      {
      range(rect, current.right, pointsInsideRect);
      }
    if (rect.contains(current.val)) 
      {
      pointsInsideRect.enqueue(current.val);
      }
    }

  // a nearest neighbor in the set to point p; null if the set is empty 
  public Point2D nearest(Point2D p) 
    {
    if (null == p) 
      {
      throw new NullPointerException();
      }
    if (isEmpty()) 
      {
      return null;
      }
    return nearest(p, root, Double.POSITIVE_INFINITY, null).getPoint();
    }

  private NearestResult nearest(final Point2D p, 
                                final Node current,
                                final double distance,
                                final Point2D closest) 
    {
    if (null == current ||
       current.nodesGrid.distanceSquaredTo(p) > distance) 
      {
      return new NearestResult(distance, closest);
      }
    double currentDistance = current.val.distanceSquaredTo(p);
    NearestResult nearestResult;
    if (currentDistance < distance) 
      {
      nearestResult = new NearestResult(currentDistance, current.val);
      } 
    else 
      {
      nearestResult = new NearestResult(distance, closest);
      }

    boolean leftFirst = false;
    if (current.isKeyVertical == VERTICAL) 
      {
      if (p.x() < current.key) 
        {
        leftFirst = true;
        }
      } 
    else 
      {
      if (p.y() < current.key) 
        {
        leftFirst = true;
        }
      }
    if (leftFirst) 
      {
      nearestResult = nearest(p, 
                              current.left, 
                              nearestResult.getDistance(),
                              nearestResult.getPoint());
      nearestResult = nearest(p, 
                              current.right, 
                              nearestResult.getDistance(),
                              nearestResult.getPoint());

      } 
    else 
      {
      nearestResult = nearest(p, 
                              current.right, 
                              nearestResult.getDistance(),
                              nearestResult.getPoint());
      nearestResult = nearest(p, 
                              current.left, 
                              nearestResult.getDistance(),
                              nearestResult.getPoint());
      }
    return nearestResult;
    }

  private static class NearestResult {

      private final double distance;
      private final Point2D point;

      public NearestResult(double distance, Point2D point) 
        {
        this.distance = distance;
        this.point = point;
        }

      public double getDistance() 
        {
        return distance;
        }

      public Point2D getPoint() 
        {
        return point;
        }
  }

  // unit testing of the methods (optional) 
  public static void main(String[] args) 
    {
    if (args.length != 1) 
      {
      throw new IllegalArgumentException("provide filename as argument");
      }
    KdTree kdTree = new KdTree();
    kdTree.readPointsFromFile(args[0]);
    Stopwatch stopwatch = new Stopwatch();
    Point2D point = new Point2D(0.534766, 0.845211);
    Point2D closestPoint = kdTree.nearest(point);
    System.out.println(closestPoint);
    double seconds = stopwatch.elapsedTime();
    System.out.printf("size:%-25d seconds: %10.5f result: %s\n",
                      kdTree.size(), 
                      seconds, 
                      closestPoint);
    RectHV rectangle = new RectHV(0.54, 0.54, 0.57, 0.57);
    Stopwatch stopwatch2 = new Stopwatch();
    Iterable<Point2D> withinRectangle = kdTree.range(rectangle);
    double seconds2 = stopwatch2.elapsedTime();
    int count = 0;
    for (Point2D point2 : withinRectangle) 
      {
      count++;
      }
    System.out.printf("size:%-25d seconds: %10.5f number of points: %d\n",
                      kdTree.size(), 
                      seconds2, 
                      count);
    }
  private void readPointsFromFile(String fileName) 
    {
    In scanner = new In(fileName);
    double[] coords = scanner.readAllDoubles();
    for (int i = 0; i < coords.length; i += 2) 
      {
      Point2D point = new Point2D(coords[i], coords[i + 1]);
      this.insert(point);
      }
    }
}
