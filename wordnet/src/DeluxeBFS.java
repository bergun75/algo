import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Queue;
import java.util.Map;
import java.util.HashMap;

public class DeluxeBFS {
    private static final int NOTCONNECTED = -1;
    private final Map<Integer, Integer> bfsStepCntMap 
                  = new HashMap<Integer, Integer>();
    private final Map<Integer, Integer> bfsStepCntMap2 
                  = new HashMap<Integer, Integer>();

    private int sap = -1;
    private int sapLength = -1;

    /**
     * Computes the shortest path from <tt>s</tt> and <tt>s2</tt> to
     * every other vertex in graph <tt>G</tt> until common ancestor found.
     * @param G the digraph
     * @param s the source vertex
     * @param s2 the second source vertex
     */
    public DeluxeBFS(Digraph G, int s, int s2) {
        bfs(G, s, s2);
    }
    /**
     * Computes the shortest path from <tt>sources</tt> and <tt>sources2</tt> to
     * every other vertex in graph <tt>G</tt> until common ancestor found.
     * @param G the digraph
     * @param sources the source vertices
     * @param sources2 the second source vertices
     */
    public DeluxeBFS(Digraph G, 
                     Iterable<Integer> sources, 
                     Iterable<Integer> sources2) {
        bfs(G, sources, sources2);
    }

    private void bfs(Digraph G, int s , int s2) {
        if (s == s2) {
            sap = s;
            sapLength = 0;
            return;
        }
        Queue<Integer> queueS = new Queue<Integer>();
        Queue<Integer> queueS2 = new Queue<Integer>();
        bfsStepCntMap.put(s, 0);
        bfsStepCntMap2.put(s2, 0);
        queueS.enqueue(s);
        queueS2.enqueue(s2);
        bfsLoop(G, queueS, queueS2);
    }
    
    private void bfs(Digraph G, 
                     Iterable<Integer> sources, 
                     Iterable<Integer> sources2) {
        Queue<Integer> queueS = new Queue<Integer>();
        Queue<Integer> queueS2 = new Queue<Integer>();
        for (int s:sources) {
            bfsStepCntMap.put(s, 0);
            queueS.enqueue(s);
        }
        for (int s2:sources2) {
            if (bfsStepCntMap.get(s2) != null) {
                sap = s2;
                sapLength = 0;
                return;
            }
            bfsStepCntMap2.put(s2, 0);
            queueS2.enqueue(s2);
        }
        bfsLoop(G, queueS, queueS2);
    }
    
    private void bfsLoop(Digraph G, 
                         Queue<Integer> queueS, 
                         Queue<Integer> queueS2) {
        while (!queueS.isEmpty() 
               || !queueS2.isEmpty()) {
            if (!queueS.isEmpty() 
                && (queueS2.isEmpty()
                    || bfsStepCntMap.get(queueS.peek()) 
                                         <= bfsStepCntMap2.get(queueS2.peek()))) {
                int v = queueS.dequeue();
                if (sapLength != -1 
                    && bfsStepCntMap.get(v) >= sapLength) {
                    break;
                }
                int q1CurrentStep = bfsStepCntMap.get(v);
                if (bfsStepCntMap2.get(v) != null) {
                    int tSapLength = bfsStepCntMap2.get(v) + q1CurrentStep;
                    if (sapLength == -1 || tSapLength < sapLength) {
                        sap = v;
                        sapLength = tSapLength;
                    }
                }
                int q1NextStep = q1CurrentStep + 1;
                if (sapLength == -1 
                    || q1NextStep < sapLength) {
                    for (int w:G.adj(v)) {
                        if (bfsStepCntMap.get(w) == null) {
                            bfsStepCntMap.put(w, q1NextStep);
                            queueS.enqueue(w);
                        }
                    }
                }
            }
            if (!queueS2.isEmpty() 
                && (queueS.isEmpty() 
                    || bfsStepCntMap2.get(queueS2.peek()) 
                       <= bfsStepCntMap.get(queueS.peek()))) {
                int v = queueS2.dequeue();
                if (sapLength != -1 
                    && bfsStepCntMap2.get(v) >= sapLength) {
                    break;
                }
                int q2CurrentStep = bfsStepCntMap2.get(v);
                if (bfsStepCntMap.get(v) != null) {
                    int tSapLength = q2CurrentStep + bfsStepCntMap.get(v);
                    if (sapLength == -1 
                        || tSapLength < sapLength) {
                        sap = v;
                        sapLength = tSapLength;
                    }
                }
                int q2NextStep = q2CurrentStep + 1;
                if (sapLength == -1 
                    || q2NextStep < sapLength) {
                    for (int w:G.adj(v)) {
                        if (bfsStepCntMap2.get(w) == null) {
                            bfsStepCntMap2.put(w, q2NextStep);
                            queueS2.enqueue(w);
                        }
                    }
                }
            }
        }
    }
    public int getSap() {
        return sap;   
    }
    
    public int getSapLength() {
        return sapLength;   
    }
}
