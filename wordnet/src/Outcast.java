import edu.princeton.cs.algs4.In;

public class Outcast {
    private final WordNet wordnet;
    public Outcast(WordNet wordnet) 
      { // constructor takes a WordNet object
      this.wordnet =  wordnet;
      }
    public String outcast(String[] nouns) 
      { 
      // given an array of WordNet nouns, return an outcast
      int maxAncDist =  -1;
      String outcast =  null;
      for (int i = 0; i < nouns.length; i++) 
        {
        int curDistSum =  0;
        for (int j = 0; j < nouns.length; j++) 
          {
          if (i != j) 
            {
            int distance = wordnet.distance(nouns[i], nouns[j]);
            if (-1 != distance) 
              {
              curDistSum += distance;
              }
            }
          if (-1 == maxAncDist || 
              curDistSum > maxAncDist) 
            {
            outcast = nouns[i];
            maxAncDist = curDistSum;
            }
          }
        }
      return outcast;
      }

    public static void main(String[] args) { // see test client below
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) 
          {
          In in = new In(args[t]);
          String[] nouns = in.readAllStrings();
          System.out.println(args[t] + ": " + outcast.outcast(nouns));
          }
    }
}
