import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.DirectedCycle;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

public class WordNet {
    private final Digraph G;
    private final SAP sap;
    // A unique synset noun can exist for various different synset entries 
    // thus synsetIds
    private final Map<String, Bag<Integer>> synsetNounMap =
                                            new HashMap<String, Bag<Integer>>();
    private final Map<Integer, Synset> synsetMap =
                                            new HashMap<Integer, Synset>();
    public WordNet(String synsets,  
                   String hypernyms) 
      {
      String [] synsetLines = readAllLinesFromFile(synsets);
      constructSynsetMaps(synsetLines);
      G = new Digraph(synsetLines.length);
      String [] hypernymLines = readAllLinesFromFile(hypernyms);
      int numberOfVerticesThatHasOutEdges = constructDigraph(hypernymLines);
      int rootCount = G.V() - numberOfVerticesThatHasOutEdges;
      validateOnlyOneRoot(rootCount);
      validateDigraph(G);
      sap = new SAP(G);
      }
           
    // returns all WordNet nouns
    public Iterable<String> nouns() 
      {
      return synsetNounMap.keySet();
      }

    // is the word a WordNet noun?
    public boolean isNoun(String word) 
      {
      if (null == word) 
        {
        throw new NullPointerException();
        }
      return synsetNounMap.containsKey(word);
      }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, 
                        String nounB) 
      {
      validateNouns(nounA, nounB);
      Bag<Integer> nounABag = synsetNounMap.get(nounA);
      Bag<Integer> nounBBag = synsetNounMap.get(nounB);
      return sap.length(nounABag, nounBBag);
      }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA 
    // and nounB in a shortest ancestral path (defined below)
    public String sap(String nounA, 
                      String nounB) 
      {
      validateNouns(nounA, nounB);
      Bag<Integer> nounABag = synsetNounMap.get(nounA);
      Bag<Integer> nounBBag = synsetNounMap.get(nounB);
      int ancestorId = sap.ancestor(nounABag, nounBBag);
      Synset found = synsetMap.get(ancestorId);
      return found.getName();
      }

    private void validateNouns(String nounA, 
                               String nounB) 
      {
      if (isNoun(nounA) && isNoun(nounB)) 
        {
        return;
        }
      throw new IllegalArgumentException();
      }

    // do unit testing of this class
    public static void main(String[] args) 
      {
      if (args.length != 4) 
        {
        throw new IllegalArgumentException();
        }
      WordNet wordnet = new WordNet(args[0], args[1]);
      System.out.printf("dist between %s and %s is %d and common ancestor=%s %n",
                        args[2], 
                        args[3], 
                        wordnet.distance(args[2], args[3]), 
                        wordnet.sap(args[2], args[3]));
      }

    private void constructSynsetMaps(String [] lines)
      {
      for (String line:lines) 
        {
        String [] tokens = line.split(",");
        int synsetId = Integer.parseInt(tokens[0]);
        String synsetNounToken = tokens[1];
        String [] synsetNouns = synsetNounToken.split("\\s+");
        updateSynsetNounMap(synsetNouns,
                            synsetId);
        synsetMap.put(synsetId, new Synset(synsetId, synsetNounToken));
        }
      }

    private String [] readAllLinesFromFile(String synsets)
      {
      In scanSynsets = new In(synsets);
      return scanSynsets.readAllLines();
      }

    // constructs digraph and returns number of verticesThatHasOutEdges
    private int constructDigraph(String [] lines)
      {
      Set<String> verticesThatHasOutEdges = new HashSet<>();
      for (String line:lines) {            
          String [] tokens = line.split(",");
          if (tokens.length > 1)
            {
            verticesThatHasOutEdges.add(tokens[0]);
            }
          for (int i = 1; i < tokens.length; i++) {
              G.addEdge(Integer.parseInt(tokens[0]), 
                        Integer.parseInt(tokens[i]));
          }
      }
      return verticesThatHasOutEdges.size();
      }
    private void validateOnlyOneRoot(final int rootCount)
      {
      if (rootCount != 1) 
        {
        throw new IllegalArgumentException(
                            String.format("not a rooted Digraph with %d roots",
                                          rootCount));
        }
      
      }
    private void validateDigraph(Digraph diG)
      {
      if (new DirectedCycle(diG).hasCycle()) 
        {
        throw new IllegalArgumentException("not a DAG");   
        }        
      }

    private void updateSynsetNounMap(final String [] synsetNouns,
                                     int synsetId)
      {
      for (String synsetNoun:synsetNouns) 
        {
        Bag<Integer> synsetIdBag = synsetNounMap.get(synsetNoun);
        if (null == synsetIdBag) 
          {
          synsetIdBag = new Bag<Integer>();
          synsetNounMap.put(synsetNoun, synsetIdBag);
          }
        synsetIdBag.add(synsetId);
        }
      }
  
    private static class Synset {
        private final int id;
        private final String name;
        Synset(int id, String name) 
          {
          this.id = id;
          this.name = name;
          }
        public int getId() 
          {
          return id;
          }
        public String getName() 
          {
          return name;
          }
    }
}
