import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Bag;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

public class SAP {
    private final Digraph G;
    private final Map<NodePair, SAPResult> mapSapResults = new HashMap<>();

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) 
      {
      this.G = new Digraph(G);
      }
    // length of shortest ancestral path between v and w; -1 if  no such path
    public int length(int v, 
                      int w) 
      {
      validateInput(v, w);
      SAPResult result = mapSapResults.get(new NodePair(v, w));
      if (null != result) 
        {
        return result.getSapLength();
        }
      DeluxeBFS bfs = new DeluxeBFS(G, 
                                    v,
                                    w);
      result = new SAPResult(bfs.getSap(),  
                             bfs.getSapLength());
      mapSapResults.put(new NodePair(v, w),  result);
      return bfs.getSapLength();
      }
    // a common ancestor of v and w that participates in a shortest ancestral path;
    // -1 if  no such path
    public int ancestor(int v, 
                        int w) {
        validateInput(v, w);
        SAPResult result = mapSapResults.get(new NodePair(v, w));
        if (null != result) 
          {
          return result.getSap();
          }
        DeluxeBFS bfs = new DeluxeBFS(G, 
                                      v, 
                                      w);
        result = new SAPResult(bfs.getSap(),  
                               bfs.getSapLength());
        mapSapResults.put(new NodePair(v, w),  result);
        return bfs.getSap();
    }
    
    private void validateInput(int v,
                               int w)
      {
      if (v >= G.V() || 
          w >= G.V()) 
        {
        throw new IndexOutOfBoundsException();
        }
      }
    // length of shortest ancestral path between any vertex in v and any vertex in w;
    //  -1 if  no such path
    public int length(Iterable<Integer> v, 
                      Iterable<Integer> w) 
      {
      int [] sizes = validateInput(v, w);
      int vSize = sizes[0];
      int wSize = sizes[1];
      SAPResult result = mapSapResults.get(new NodePair(v,  
                                                        vSize,  
                                                        w,  
                                                        wSize));
      if (null != result) 
        {
        return result.getSapLength();
        }
      DeluxeBFS bfs = new DeluxeBFS(G, 
                                    v,
                                    w);
      result = new SAPResult(bfs.getSap(),  
                             bfs.getSapLength());
      mapSapResults.put(new NodePair(v, 
                                     vSize, 
                                     w, 
                                     wSize),  
                                     result);
      return bfs.getSapLength();
      }
    // a common ancestor that participates in shortest ancestral path; -1 if
    //  no such path
    public int ancestor(Iterable<Integer> v,  
                        Iterable<Integer> w) 
      {
      int [] sizes = validateInput(v, w);
      int vSize = sizes[0];
      int wSize = sizes[1];
      SAPResult result = mapSapResults.get(new NodePair(v,  
                                                        vSize,  
                                                        w,  
                                                        wSize));
      if (null != result) 
        {
        return result.getSap();
        }
      DeluxeBFS bfs = new DeluxeBFS(G, 
                                    v, 
                                    w);
      result = new SAPResult(bfs.getSap(),  
                             bfs.getSapLength());
      mapSapResults.put(new NodePair(v, 
                                     vSize, 
                                     w, 
                                     wSize),  
                                     result);
      return bfs.getSap();
      }
    
    private int [] validateInput(Iterable<Integer> v,  
                                 Iterable<Integer> w)
      {
      int vSize = 0;
      for (int s:v) 
        {
        if (s >= G.V()) 
          {
          throw new IndexOutOfBoundsException();
          }
        vSize++;
        }
      int wSize = 0;
      for (int s:w) 
        {
        if (s >= G.V()) 
          {
          throw new IndexOutOfBoundsException();
          }
        wSize++;
        }
      return new int[] {vSize, wSize};
      }
    
    // do unit testing of this class
    public static void main(String[] args) 
      {
      if (args.length < 3) 
        {
        throw new IllegalArgumentException();
        }
      In scanner = new In(args[0]);
      Digraph G = new Digraph(scanner);
      System.out.println(G);
      SAP sap = new SAP(G);
      String [] vs = args[1].split(",");
      String [] ws = args[2].split(",");
      Bag<Integer> bagV = new Bag<Integer>();
      Bag<Integer> bagW = new Bag<Integer>();
      for (String v:vs) 
        {
        bagV.add(Integer.parseInt(v));
        }
      for (String w:ws) 
        {
        bagW.add(Integer.parseInt(w));
        }
      System.out.printf("v=%s,  w=%s,  ancestor=%d,  anclength=%d%n", 
      args[1], args[2], sap.ancestor(bagV, bagW), sap.length(bagV, bagW));
      System.out.printf("v=%s,  w=%s,  ancestor=%d,  anclength=%d%n", 
      args[2], args[1], sap.ancestor(bagW, bagV), sap.length(bagW, bagV));
      }
    private static class NodePair 
        {
        private final int [] vS;
        private final int [] wS;
        NodePair(int v,  
                 int w) 
          {
          vS = new int [1];
          vS[0] = v;
          wS = new int [1];
          wS[0] = w;
          }
        NodePair(Iterable<Integer> vS, 
                 int vSize,  
                 Iterable<Integer> wS,  
                 int wSize) 
          {
          this.vS = new int [vSize];
          int i = 0;
          for (int v:vS) 
            {
            this.vS[i] = v;
            i++;
            }
          i = 0;
          this.wS = new int [wSize];
          for (int w:wS) 
            {
            this.wS[i] = w;
            i++;
            }
          }
        public boolean equals(Object o) 
          {
          if (null == o) 
            {
            return false;
            }
          if (!(o.getClass().isInstance(this))) 
            {
            return false;
            }
          NodePair obj = (NodePair) o;
          if (!Arrays.equals(this.vS,  obj.vS)) 
            {
            return false;
            }
          if (!Arrays.equals(this.wS,  obj.wS)) 
            {
            return false;
            }
          return true;
          }
        
        public int hashCode() 
          {
          int prime = 31;
          int result = 1;
          result = prime * result + Arrays.hashCode(vS);
          result = prime * result + Arrays.hashCode(wS);
          return result;
          }
    }
    private static class SAPResult 
      {
      private final int sapLength;
      private final int sap;
      SAPResult(int sap, 
                int sapLength) 
        {
        this.sap = sap; 
        this.sapLength = sapLength; 
        }
      public int getSap() 
        {
        return sap;
        }
      public int getSapLength() 
        {
        return sapLength;
        }
      }
}

