import edu.princeton.cs.algs4.Picture;
import java.awt.Color;

public class SeamCarver 
  {
  private int [][] colorArray;
  private double [][] energySumArray;
  private boolean transposed = false;
  public SeamCarver(Picture picture)
  // create a seam carver object based on the given picture
    {
    if (null == picture)
      {
      throw new NullPointerException();
      }
    this.colorArray = new int[picture.height()][picture.width()];
    for (int row = 0; row < picture.height(); row++)
      {
      for (int col = 0; col < picture.width(); col++)
        {
        this.colorArray[row][col] = picture.get(col, row).getRGB();
        }
      }
    
    energySumArray = new double[height()][width()];
    initializeEnergyArray();
    traverse();
    }
  public static void main(String...args)
    {
    }
  public Picture picture()
  // current picture
    {
    if (transposed)
      {
      transpose();
      }
    int width = widthInternal();
    int height = heightInternal();
    Picture picture = new Picture(width, height);
    for (int row = 0; row < height; row++)
      {
      for (int col = 0; col < width; col++)
        {
        picture.set(col, row, new Color(colorArray[row][col]));
        }
      }    
    return picture;
    }
  public int width()
  // width of current picture
    {
    if (transposed)
      {
      return heightInternal();
      }
    else
      {
      return widthInternal();
      }
    }
  public int height()
  // height of current picture
    {
    if (!transposed)
      {
      return heightInternal();
      }
    else
      {
      return widthInternal();
      }
    }
  private int widthInternal()
    {
    if (null == colorArray || null == colorArray[0])
      {
      return 0;
      }
    return colorArray[0].length;
    }
  private int heightInternal()
    {
    if (null == colorArray)
      {
      return 0;
      }
    return colorArray.length;
    }
  public double energy(int col, int row)
  // energy of pixel at column x and row y
    {
    if (!transposed)
      {
      return energyInternal(col, row);
      }
    else
      {
      return energyInternal(row, col);
      }
    }
  private double energyInternal(int col, int row)
    {
    int w = widthInternal();
    int h = heightInternal();
    if (col < 0 || col >= w || row < 0 || row >= h)
      {
      throw new IndexOutOfBoundsException(
      String.format("col=%d, row=%d, width=%d, height=%d", col, row, w, h));
      }
    if (col == 0 || col == w-1 || row == 0 || row == h-1)
      {
      return 1000.0;
      }
    return rootOfSquareSums(col, row);
    }
  private double rootOfSquareSums(int col, int row)
    {
    int rowcolminus1 = colorArray[row][col-1];
    int rowcolplus1 = colorArray[row][col+1];
    int colrowminus1 = colorArray[row-1][col];
    int colrowplus1 = colorArray[row+1][col];
    int rxDiff = getRed(colrowplus1) - getRed(colrowminus1);
    int gxDiff = getGreen(colrowplus1) - getGreen(colrowminus1);
    int bxDiff = getBlue(colrowplus1) - getBlue(colrowminus1);
    int ryDiff = getRed(rowcolplus1) - getRed(rowcolminus1);
    int gyDiff = getGreen(rowcolplus1) - getGreen(rowcolminus1);
    int byDiff = getBlue(rowcolplus1) - getBlue(rowcolminus1);
    return Math.sqrt((rxDiff*rxDiff + gxDiff*gxDiff + bxDiff*bxDiff)
                    +(ryDiff*ryDiff + gyDiff*gyDiff + byDiff*byDiff)
                    );
    }
  private int getBlue(int rgb)
    {
     return (rgb >> 0) & 0xFF;
    }
  private int getGreen(int rgb)
    {
     return (rgb >> 8) & 0xFF;
    }
  private int getRed(int rgb)
    {
     return (rgb >> 16) & 0xFF;
    }
  public int[] findHorizontalSeam()
  // sequence of indices for horizontal seam
    {
    if (!transposed)
    {
    transpose();
    }
    return findSeam();
    }
  public int[] findVerticalSeam()
  // sequence of indices for vertical seam
    {
    if (transposed)
      {
      transpose();
      }
    return findSeam();
    }

  private int [] findSeam()
    {
    int [] seam = new int[heightInternal()];
    int curRow = heightInternal()-1;
    double lowestEnergy = Double.POSITIVE_INFINITY;
    int curCol = 0;
    for (int col = 0; col < widthInternal(); col++)
      {
      if (energySumArray[curRow][col] < lowestEnergy)
        {
        lowestEnergy = energySumArray[curRow][col];
        curCol = col;
        }
      }
    seam[curRow] = curCol;
    curRow--;
    
    while (curRow >= 0)
      {
      lowestEnergy = Double.POSITIVE_INFINITY;
      int tmpCol = curCol;
      for (int col1 = tmpCol - 1; col1 <= tmpCol + 1; col1++)
        {
        if (col1 >= 0 && col1 < energySumArray[0].length)
          {
          if (energySumArray[curRow][col1] < lowestEnergy)
            {
            lowestEnergy = energySumArray[curRow][col1];
            curCol = col1;
            }
          }
        }
      seam[curRow] = curCol;
      curRow--;
      }
    return seam;
    }

  private void traverse()
    {
    for (int row = 0; row < heightInternal()-1; row++)
      {
      for (int col = 1; col < widthInternal()-1; col++)
        {
        traverseNeighbors(col, row);
        }
      }    
    }

  private void initializeEnergyArray()
    {
    for (int row = 0; row < heightInternal(); row++)
      {
      for (int col = 0; col < widthInternal(); col++)
        {
        if (row == 0)
          {
          energySumArray[row][col] = 1000.0;
          }
        else
          {
          energySumArray[row][col] = Double.POSITIVE_INFINITY;
          }
        }
      }
    }

  private void traverseNeighbors(int col, int row)
    {
    int downRow = row + 1;
    int leftCol = col - 1;
    int rightCol = col + 1;
    
    updateCellEnergy(col, row, leftCol, downRow);
    updateCellEnergy(col, row, col, downRow);
    updateCellEnergy(col, row, rightCol, downRow);
    }

  private void updateCellEnergy(int existingCol,
                                int existingRow,
                                int updateCol,
                                int updateRow)
    {
    double sum = energySumArray[existingRow][existingCol]
                 + energyInternal(updateCol, updateRow);
    if (sum < energySumArray[updateRow][updateCol])
      {
      energySumArray[updateRow][updateCol] = sum;
      }  
    }

  public void removeHorizontalSeam(int[] seam)
  // remove horizontal seam from current picture
    {
    if (!transposed)
    {
    transpose();
    }
    removeSeam(seam);
    }
  public void removeVerticalSeam(int[] seam)
  // remove vertical seam from current picture
    {
    if (transposed)
      {
      transpose();
      }
    removeSeam(seam);
    }
  private void removeSeam(int [] seam)
    {
    validateSeam(seam);
    int newWidth = widthInternal()-1;
    for (int i = 0; i < seam.length; i++)
      {
      int [] tempColorArray = new int[newWidth];
      double [] tempEnergySumArray = new double[newWidth];
      System.arraycopy(this.colorArray[i], 
                       0, 
                       tempColorArray, 
                       0, 
                       seam[i]);
      System.arraycopy(this.energySumArray[i], 
                       0, 
                       tempEnergySumArray, 
                       0, 
                       seam[i]);
      System.arraycopy(this.colorArray[i], 
                       seam[i]+1, 
                       tempColorArray, 
                       seam[i], 
                       newWidth - seam[i]);
      System.arraycopy(this.energySumArray[i], 
                       seam[i]+1, 
                       tempEnergySumArray,
                       seam[i], 
                       newWidth - seam[i]);
      this.colorArray[i] = tempColorArray;
      }
      initializeEnergyArray();
      traverse();
    }
  private void transpose()
    {
    
    int h = widthInternal();
    int w = heightInternal();
    int [][] newColorArray = new int[h][w];
    double [][] newEnergySumArray = new double[h][w];
    for (int row = 0; row < h; row++)
      {
      for (int col = 0; col < w; col++)
        {
            newColorArray[row][col] = this.colorArray[col][row];
        }
      }
    this.colorArray = newColorArray;
    this.energySumArray = newEnergySumArray;
    initializeEnergyArray();
    traverse();
    transposed = !transposed;
    }  
  private void validateSeam(int [] seam)
    {
    if (null == seam)
      {
      throw new NullPointerException();
      }
    if (seam.length != heightInternal())
      {
      throw new IllegalArgumentException(
      String.format("invalid seam length=%d height=%d transposed=%b", 
                     seam.length, heightInternal(), transposed));
      }
    if (widthInternal() <= 1)
      {
      throw new IllegalArgumentException(
          String.format("no more carving possible width=%d transposed=%b",
                          widthInternal(), transposed));
      }
    if (seam[0] < 0 || seam[0] >= widthInternal())
      {
      throw new IllegalArgumentException(
          String.format("Invalid seam=%d width=%d", seam[0], widthInternal()));
      }
    for (int i = 1; i < seam.length; i++)
      {
      if (seam[i] < 0 || seam[i] >= widthInternal())
        {
        throw new IllegalArgumentException(
          String.format("Invalid seam=%d width=%d", seam[i], widthInternal()));
        }
      if (Math.abs(seam[i]-seam[i-1]) > 1)
        {
        throw new IllegalArgumentException(
          String.format("Invalid seams %d,%d on indexes %d,%d"
                        , seam[i], seam[i-1], i, i-1));
        }
      }
    }
  }
