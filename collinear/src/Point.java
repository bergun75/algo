/**
 *
 * @author bergun
 */
import java.util.Comparator;
import edu.princeton.cs.algs4.StdDraw;

public class Point implements Comparable<Point> 
  {
  private final short x;                              // x coordinate
  private final short y;                              // y coordinate

  // create the point (x, y)
  public Point(int x, int y) 
    {
    /* DO NOT MODIFY */
    this.x = (short) x;
    this.y = (short) y;
    }

  // plot this point to standard drawing
  public void draw() 
    {
    /* DO NOT MODIFY */
    StdDraw.point(x, y);
    }

  // draw line between this point and that point to standard drawing
  public void drawTo(Point that) 
    {
    /* DO NOT MODIFY */
    StdDraw.line(this.x, this.y, that.x, that.y);
    }

  // slope between this point and that point
  public double slopeTo(Point that) 
    {
    if (Double.compare(this.x, that.x) == 0 && 
        Double.compare(this.y, that.y) == 0) 
      {
      return Double.NEGATIVE_INFINITY;
      }
    double yDiff = that.y - this.y;
    double xDiff = that.x - this.x;
    if (Double.compare(xDiff, 0.0) == 0) 
      {
      return Double.POSITIVE_INFINITY;
      }
    if (Double.compare(yDiff, 0.0) == 0) 
      {
      return 0.0;
      } 
    else 
      {
      return yDiff / xDiff;
      }
    }

  // is this point lexicographically smaller than that one?
  // comparing y-coordinates and breaking ties by x-coordinates
  public int compareTo(Point that) 
    {
    if ((Double.compare(this.y, that.y) < 0) || 
        (
         Double.compare(this.y, that.y) == 0 && 
         Double.compare(this.x, that.x) < 0
        )
       ) 
      {
      return -1;
      } 
    else if (Double.compare(this.y, that.y) > 0 || 
             (
              Double.compare(this.y, that.y) == 0 && 
              Double.compare(this.x, that.x) > 0
             )
            ) 
      {
      return 1;
      }
    return 0;
    }

  public Comparator<Point> slopeOrder()
    {
    return new Comparator<Point>() 
      {
      @Override
      public int compare(Point o1, 
                         Point o2) 
        {
        double slopeToO1 = slopeTo(o1);
        double slopeToO2 = slopeTo(o2);
        if (Double.compare(slopeToO1, slopeToO2) < 0) 
          {
          return -1;
          } 
        else if (Double.compare(slopeToO1, slopeToO2) == 0) 
          {
          return 0;
          }
        return 1;
        }
      };
    }

  // return string representation of this point
  public String toString() 
    {
    /* DO NOT MODIFY */
    return "(" + x + ", " + y + ")";
    }

  // unit test
  public static void main(String[] args) 
    {
    Point p = new Point(23687, 11123);
    Point q = new Point(21470, 254);
    System.out.println(q.slopeTo(p));
    System.out.println(p.slopeTo(q));
    }
}
