/**
 *
 * @author bergun
 */

import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Merge;
import edu.princeton.cs.algs4.In;
import java.util.List;
import java.util.LinkedList;

public class BruteCollinearPoints 
  {
  /**
   * @param args the command line arguments
   */
  private final Point[] points;

  private LineSegment[] segments;

  public BruteCollinearPoints(Point[] points)
    {
    if (null == points) 
      {
      throw new NullPointerException();
      }
    this.points = points.clone();
    Merge.sort(this.points);
    Point pointPrevious = null;
    for (int i = 0; i < points.length; i++)
      {
      if (null != pointPrevious)
        {
        if (this.points[i].compareTo(pointPrevious) == 0)
          {
          throw new IllegalArgumentException();
          }
        }
      pointPrevious = this.points[i];
      }
    }
  public LineSegment[] segments() 
    {
    if (null == segments)
      {
      segments = discoverSegments();
      }
    return segments.clone();
    }

  public int numberOfSegments()
    {
    if (null == segments)
      {
      segments = discoverSegments();
      }
    return segments.length;
    }

  private LineSegment[] discoverSegments()
    {
    List<LineSegment> segmentList = new LinkedList<>();
    for (int i = 0; i < points.length; i++) 
      {
      for (int j = i + 1; j < points.length; j++) 
        {
        for (int k = j + 1; k < points.length; k++) 
          {
          for (int l = k + 1; l < points.length; l++) 
            {
            // TODO use Double.compare approach
            if (points[i].slopeTo(points[j])
                    == points[j].slopeTo(points[k])
                    && points[j].slopeTo(points[k])
                    == points[k].slopeTo(points[l])) 
              {
              LineSegment segment = new LineSegment(points[i], points[l]);
              segmentList.add(segment);
              }
            }
          }
        }
      }
    return segmentList.toArray(new LineSegment[0]);
    }

  public static void main(String[] args) 
    {
    if (args.length != 1 || null == args[0]) 
      {
      throw new IllegalArgumentException();
      }
    Point[] points = readAllPointsFromFile(args[0]);
    drawPoints(points);
    drawLineSegments(points);
    }
  private static Point[] readAllPointsFromFile(String fileName)
    {
    In scanner = new In(fileName);
    int n = scanner.readInt();
    Point[] pointos = new Point[n];
    for (int i = 0; i < pointos.length; i++) 
      {
      int x = scanner.readInt();
      int y = scanner.readInt();
      if (x < 0 || x > 32768 || y < 0 || y > 32768) 
        {
        throw new IllegalArgumentException();
        }
      pointos[i] = new Point(x, y);
      }
    return pointos;
    }

  private static void drawPoints(Point[] points)
    {
    // draw the points
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    StdDraw.setPenRadius(0.01);
    for (Point p : points)
      {
      p.draw();
      }
    StdDraw.show();
    }
  private static void drawLineSegments(Point[] points)
    {
    // print and draw the line segments
    BruteCollinearPoints collinear = new BruteCollinearPoints(points);
    for (LineSegment segment : collinear.segments()) 
      {
      StdOut.println(segment);
      segment.draw();
      }
    StdDraw.show();
    }
}
