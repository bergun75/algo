import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Merge;
import edu.princeton.cs.algs4.In;

/**
 *
 * @author bergun
 */
public class FastCollinearPoints
  {
  private final Point[] points;

  private LineSegment[] segments;

  public FastCollinearPoints(Point[] points)
    {
    if (null == points)
      {
      throw new NullPointerException();
      }
    this.points = points.clone();
    Merge.sort(this.points);    
    Point pointPrevious = null;
    for (int i = 0; i < points.length; i++)
      {
      if (null != pointPrevious)
        {
        if (this.points[i].compareTo(pointPrevious) == 0)
          {
          throw new IllegalArgumentException();
          }
        }
      pointPrevious = this.points[i];
      }
    }
  public LineSegment[] segments()
    {
    if (null == segments)
      {
      segments = discoverSegments();
      }
    return segments.clone();
    }

  public int numberOfSegments()
    {
    if (null == segments)
      {
      segments = discoverSegments();
      }
    return segments.length;
    }


  private LineSegment[] discoverSegments() 
    {
    List<LineSegment> lineSegmentList = new LinkedList<>();    
    Point[] pointsSlopeOrder = Arrays.copyOf(this.points,
                                             this.points.length);
    for (int i = 0; i < this.points.length; i++) 
      {
      Arrays.sort(pointsSlopeOrder, this.points[i].slopeOrder());
      List<LineSegment> lsList =  findLineSegments(this.points[i], pointsSlopeOrder);
      lineSegmentList.addAll(lsList);
      }
    return lineSegmentList.toArray(new LineSegment[0]);
    }
  
  private List<LineSegment> findLineSegments(Point reference,
                                             Point[] pointsSlopeOrder) 
    {
    List<LineSegment> lineSegments = new LinkedList<>();
    int lineCount = 0;
    double prevSlope = Double.NEGATIVE_INFINITY;
    double ignoreSlope = Double.NEGATIVE_INFINITY;
    Point biggestPoint = null;
    for (int i = 0; i < pointsSlopeOrder.length; i++) 
      {
      Point curPoint = pointsSlopeOrder[i];      
      double slopeTo = reference.slopeTo(curPoint);
      if (Double.compare(slopeTo, ignoreSlope) == 0) 
      // This will also ignore Negative infinity in the begining.
        {
        continue;
        }
      if (reference.compareTo(curPoint) < 0)
      // we always consider the case where reference is the smallest 
      // of the points on line
        {
        if (Double.compare(prevSlope, Double.NEGATIVE_INFINITY) == 0)
          {
          lineCount++;
          biggestPoint = curPoint;
          }
        else if (Double.compare(prevSlope, slopeTo) == 0) 
          {
          lineCount++;
          if (biggestPoint == null || curPoint.compareTo(biggestPoint) > 0)
            {
            biggestPoint = curPoint;
            }
          }
        else
          {
          if (lineCount > 2)
            {
            lineSegments.add(new LineSegment(reference, 
                                             biggestPoint)
                            );
            }
          biggestPoint = curPoint;
          lineCount = 1;
          }
        }
      else
        {
        if (lineCount > 2 && Double.compare(prevSlope, slopeTo) != 0)
        // reference maybe smaller than other current point but
        // they maybe on different slopes so dont forget to
        // process the previous found points on a slope
        // that forms a line segment
          {
          lineSegments.add(new LineSegment(reference, 
                                           biggestPoint)
                          );
          }
        lineCount = 1;
        // cause this reference point is bigger than other points on line
        // we will ignore this slope for consideration
        ignoreSlope = slopeTo;
        }
      prevSlope = slopeTo;
      }
    // When the iteration on slope ordered points terminates we
    // still have to check for if there is a LineSegment on the last slope.
    if (lineCount > 2)
      {
      lineSegments.add(new LineSegment(reference, biggestPoint));
      }
    return lineSegments;
    }
  public static void main(String[] args)
    {
    if (args.length != 1 || null == args[0])
      {
      throw new IllegalArgumentException();
      }
    Point[] points = readAllPointsFromFile(args[0]);
    drawPoints(points);
    drawLineSegments(points);
    }
  private static Point[] readAllPointsFromFile(String fileName)
    {
    In scanner = new In(fileName);
    int n = scanner.readInt();
    Point[] pointos = new Point[n];
    for (int i = 0; i < pointos.length; i++)
      {
      int x = scanner.readInt();
      int y = scanner.readInt();
      if (x < 0 || x > 32768 || y < 0 || y > 32768)
        {
        throw new IllegalArgumentException();
        }
      pointos[i] = new Point(x, y);
      }
    return pointos;
    }
    
  private static void drawPoints(Point[] points)
    {
    // draw the points 
    StdDraw.setXscale(0, 32768);
    StdDraw.setYscale(0, 32768);
    StdDraw.setPenRadius(0.01);
    for (Point p : points) 
      {
      p.draw();
      }
    StdDraw.show();
    }
  private static void drawLineSegments(Point[] points)
    {
    // print and draw the line segments
    StdDraw.setPenRadius(0.005);
    FastCollinearPoints collinear = new FastCollinearPoints(points);
    for (LineSegment segment : collinear.segments())
      {
      StdOut.println(segment);
      segment.draw();
      }
    StdDraw.show();
    }
}
