
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bergun
 */
public class MostUsedWords {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        if (args.length != 1) {
            throw new IllegalArgumentException();
        }
        Path pathToFile = Paths.get(args[0]);
        if (Files.notExists(pathToFile)) {
            throw new FileNotFoundException();
        }
        String content = new String(Files.readAllBytes(pathToFile));
        String[] words = content.split("\\W\\s*|\\s+|\\r?\\n");
        TreeMap<String, Integer> wordMap = new TreeMap<>();
        PriorityQueue<Word> sortedAccordingCount = new PriorityQueue<>();        
        for (String word : words) {
            if (wordMap.containsKey(word)) {
                Integer occurenceCnt = wordMap.get(word);
                wordMap.put(word, ++occurenceCnt);
                Word wordObj = new Word(word,occurenceCnt);
                //sortedAccordingCount.remove(wordObj);
                sortedAccordingCount.offer(wordObj);                
            } else {
                wordMap.put(word, 1);
                Word wordObj = new Word(word,1);
                sortedAccordingCount.offer(wordObj);                
            }

        }
        int count = 0;
        while(count ++<5){
            Word word = sortedAccordingCount.poll();
            System.out.printf("word:%s occurs:%d times \n", word.getWord(), word.getOccurenceCount());
        }
            

        

    }

}

class Word implements Comparable<Word> {

    private String word;

    private int occurenceCount;

    public Word(String word, int occurenceCount) {
        this.word = word;
        this.occurenceCount = occurenceCount;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getOccurenceCount() {
        return occurenceCount;
    }

    public void setOccurenceCount(int occurenceCount) {
        this.occurenceCount = occurenceCount;
    }

    @Override
    public int compareTo(Word o) {
        if (this.occurenceCount < o.occurenceCount) {
            return 1;
        }
        if (this.occurenceCount > o.occurenceCount) {
            return -1;
        }
        return 0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.word);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Word other = (Word) obj;
        if (!Objects.equals(this.word, other.word)) {
            return false;
        }
        return true;
    }

}
