public class TaxiCab
  {
  public static void main(String ... args)
    {
    int n = Integer.parseInt(args[0]);
    int pad = 1;
    for (int i = 1; i <= n; i++)
      {
      int iPow = (int) (Math.pow(i, 3));
      String padString = "%" + Integer.toString(pad)  +  "s";
      System.out.printf(padString, " ");
      for (int j = i + 1; j <= n; j++)
        {
        System.out.printf("%7d",iPow + (int) Math.pow(j, 3));
        }
      System.out.println();
      pad += 7;
      }
    }
  }
