/**
 * **************************************************************************
 * Compilation: javac PercolationStats.java Execution: java PercolationStats
 * <gridSize> <repeatCOunt>
 * Dependencies: StdRandom.java StdStats.java
 *
 * Percolation Test Statistics
 *
 ***************************************************************************
 */
/**
 * The <tt>PercolationStats</tt> class represents a benchmark environment for
 * Monte Carlo Simulation over Percolation class.
 *
 * @author Baris Ergun
 */

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private final int testGridSize;
    private final int testRepeatCount;
    private final double[] testResultsArray;

    /**
     * Initializes T independent experiments on an N-by-N grid
     *
     * @throws java.lang.IllegalArgumentException if N <= 0 || T <= 0
     * @param N size of percolation grid
     * @param T test repeat count
     */
    public PercolationStats(int N, int T) 
      {
      if (N <= 0 || T <= 0) 
        {
        throw new IllegalArgumentException();
        }
      testGridSize = N;
      testRepeatCount = T;
      testResultsArray = new double[testRepeatCount];
      run();
      }

    /**
     * runs the initialized experiment
     */
    private void run() 
      {
      for (int i = 0; i < testRepeatCount; i++) 
        {
        int numberOfOpenSites = 0;
        Percolation percolation = new Percolation(testGridSize);
        while (!percolation.percolates()) 
          {
          int row = StdRandom.uniform(1, testGridSize + 1);
          int column = StdRandom.uniform(1, testGridSize + 1);
          if (!percolation.isOpen(row, column)) 
            {
            percolation.open(row, column);
            numberOfOpenSites++;
            }
          }
        testResultsArray[i] = (double) numberOfOpenSites
                / (double) (testGridSize * testGridSize);
        }
      }

    /**
     * Returns the mean average of test results.
     *
     * @return calculated mean <tt>mean</tt>
     */
    public double mean() 
      {
      return StdStats.mean(testResultsArray);
      }

    /**
     * Returns the standard deviation of test results.
     *
     * @return calculated stdDev <tt>stdDev</tt>
     */
    public double stddev() 
      {
      return StdStats.stddev(testResultsArray);
      }

    /**
     * Returns low endpoint of 95% confidence interval
     *
     * @return calculated confidenceLo
     */
    public double confidenceLo() 
      {
      // ok should find a better way of not losing double bits when
      // calculating. But cant use BigDecimal!
      return mean() - ((1.96 * stddev()) / Math.sqrt(testRepeatCount));
      }

    /**
     * Returns high endpoint of 95% confidence interval
     *
     * @return calculated confidenceHi
     */
    public double confidenceHi() 
      {
      // ok should find a better way of not losing double bits when
      // calculating. But cant use BigDecimal!
      return mean() + ((1.96 * stddev()) / Math.sqrt(testRepeatCount));
      }

    public static void main(String[] args) 
      {
      if (args.length != 2) 
        {
        throw new IllegalArgumentException("Usage: java PercolationStats "
                + "<gridSize> <repeatCount>");
        }
      int gridSize = Integer.parseInt(args[0]);
      int testRepeatCount = Integer.parseInt(args[1]);
      PercolationStats stats = new PercolationStats(gridSize, 
                                                    testRepeatCount);
      System.out.format("%-25s = %g\n", "mean", stats.mean());
      System.out.format("%-25s = %g\n", "stddev", stats.stddev());
      System.out.format("%-25s = %g, %g\n", "95% confidence interval",
              stats.confidenceLo(), stats.confidenceHi());

      }
}
