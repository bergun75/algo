/**
 * **************************************************************************
 * Compilation: javac Percolation.java Execution: java Percolation
 *
 * Dependencies: WeightedQuickUnionUF.java
 *
 * Percolation Data Type
 *
 ***************************************************************************
 */
/**
 * The <tt>Percolation</tt> class represents a Percolation data structure.
 *
 * @author Baris Ergun
 */

import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import java.util.Set;
import java.util.HashSet;

public class Percolation {

    private final int virtualTopSiteId;

    private final int gridSize;

    // flattened 2D  array of booleans to check if the Site was opened
    private final boolean[] percolationSiteStatuses;

    private final Set<Integer> openBottomSites;

    private final WeightedQuickUnionUF percolationUF;

    /**
     * Initializes N-by-N grid, with all sites blocked{}
     *
     * @throws java.lang.IllegalArgumentException if N <= 0
     * @param N size of percolation grid
     */
    public Percolation(int N) 
      {
      if (0 >= N) {
          throw new IllegalArgumentException("N must be larger than 0");
      }      
      gridSize = N;
      // + 1 for virtual sites
      percolationUF = new WeightedQuickUnionUF((N * N) + 1);
      virtualTopSiteId = N * N;
      percolationSiteStatuses = new boolean[N*N];
      openBottomSites = new HashSet<>();

      }

    public static void main(String[] args) {
        Percolation testPercolation = new Percolation(3);
        testPercolation.open(1, 1);
        System.out.println(testPercolation.percolates());
        System.out.println(testPercolation.isFull(1, 1));
        System.out.println(testPercolation.isOpen(1, 1));
        testPercolation.open(2, 1);
        System.out.println(testPercolation.percolates());
        System.out.println(testPercolation.isFull(2, 1));
        System.out.println(testPercolation.isOpen(2, 1));
        testPercolation.open(2, 3);
        System.out.println(testPercolation.percolates());
        System.out.println(testPercolation.isFull(2, 3));
        System.out.println(testPercolation.isOpen(2, 3));
        testPercolation.open(3, 1);
        System.out.println(testPercolation.percolates());
        System.out.println(testPercolation.isFull(3, 1));
        System.out.println(testPercolation.isOpen(3, 1));
        testPercolation.open(3, 3);
        System.out.println(testPercolation.percolates());
        System.out.println(testPercolation.isFull(3, 3));
        System.out.println(testPercolation.isOpen(3, 3));
    }

    /**
     * open site (row i, column j) if it is not open already
     *
     * @param rowEx
     *
     * @param columnEx
     */
    public void open(int rowEx, int columnEx) 
      {
      int row = rowEx - 1;
      int column = columnEx - 1;
      validateSite(row, column);
      int siteId = calculateSiteId(row, column);
      percolationSiteStatuses[siteId] = true;
      processVirtualSiteUnion(row, siteId);
      processOpenBottomSites(row, column, siteId);
      processAdjacentSites(row, column, siteId);
      }

    /**
     * unite row with VIRTUAL_TOP_SITE_ID if row == 0, unite row with
     * VIRTUAL_BOTTOM_SITE_ID if row == GRID_SIZE - 1
     *
     * @param row
     *
     * @param siteId siteId for Weighted union structure
     */
    private void processVirtualSiteUnion(int row, int siteId) 
      {
      if (row == 0) 
        {
          percolationUF.union(virtualTopSiteId, siteId);
        }
      }
    
    private void processOpenBottomSites(int row, int column, int siteId)
      {
      if (row == gridSize -1)
        {
        if(column > 0 && column < gridSize - 1)
          {
          if (!percolationSiteStatuses[siteId - 1] &&
              !percolationSiteStatuses[siteId + 1])
            {
            openBottomSites.add(siteId);
            }
          }
        else if(column == 0)
          {
          if (!percolationSiteStatuses[siteId + 1])
            {
            openBottomSites.add(siteId);
            }

          }
        else if(column == gridSize -1)
          {
          if (!percolationSiteStatuses[siteId - 1])
            {
            openBottomSites.add(siteId);
            }
          }
        }
      }

    /**
     * unite with open adjacent sites
     *
     * @param row
     *
     * @param column
     *
     * @param siteId siteId for Weighted union structure
     */
    private void processAdjacentSites(int row, int column, int siteId) 
      {
      if (row > 0 && 
          percolationSiteStatuses[calculateSiteId(row, column)]) 
        {
        int leftSiteId = calculateSiteId(row - 1, column);
        union(siteId, leftSiteId);
        }
      if (row < gridSize - 1 && 
          percolationSiteStatuses[calculateSiteId(row + 1, column)]) 
        {
        int rightSiteId = calculateSiteId(row + 1, column);
        union(siteId, rightSiteId);
        }

      if (column > 0 && 
          percolationSiteStatuses[calculateSiteId(row, column - 1)]) 
        {
        int downSiteId = calculateSiteId(row, column - 1);
        union(siteId, downSiteId);
        }

      if (column < gridSize - 1 && 
          percolationSiteStatuses[calculateSiteId(row, column + 1)]) 
        {
        int upSiteId = calculateSiteId(row, column + 1);
        union(siteId, upSiteId);
        }
      }

    private void union(int site1Id, int site2Id)
      {
      percolationUF.union(site1Id, site2Id);
      }

    private int calculateSiteId(int row, int column) 
      {
      return column + row * gridSize;
      }

    /**
     * is site (row i, column j) open?.
     *
     * @param rowEx
     *
     * @param columnEx
     *
     * @return true if site is open
     *
     *
     */
    public boolean isOpen(int rowEx, int columnEx) 
      {
      int row = rowEx - 1;
      int column = columnEx - 1;
      validateSite(row, column);
      return !isBlocked(row, column);
      }

    /**
     * is site (row i, column j) full?.
     *
     * @param rowEx
     *
     * @param columnEx
     *
     * @return true if site is full
     *
     *
     */
    public boolean isFull(int rowEx, int columnEx) 
      {
      int row = rowEx - 1;
      int column = columnEx - 1;
      validateSite(row, column);
      if (isBlocked(row, column)) 
        {
        return false;
        }
      return isConnectedToTop(row, column);
      }

    private boolean isBlocked(int row, int column) 
      {
      return !percolationSiteStatuses[calculateSiteId(row, column)];
      }

    private boolean isConnectedToTop(int row, int column) 
      {
      return percolationUF.connected(virtualTopSiteId,
              calculateSiteId(row, column));
      }

    private void validateSite(int row, int column) 
      {
      if (row >= gridSize || column >= gridSize || row < 0 || column < 0) 
        {
        throw new IndexOutOfBoundsException();
        }
      }

    /**
     * does the system percolate?
     *
     * @return true if system percolates
     *
     */
    public boolean percolates() 
      {
      for (int bottomSite:openBottomSites)
        {
        if(percolationUF.connected(virtualTopSiteId,
              bottomSite))
          {
          return true;
          }
        }
      return false;
      }

}
