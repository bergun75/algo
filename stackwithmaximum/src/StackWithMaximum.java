
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bergun
 */
public class StackWithMaximum<Item> implements Iterable<Item> {

    /**
     * Is this stack empty?
     *
     * @return true if this stack is empty; false otherwise
     */
    private final Stack<Item> stack;

    private final Stack<Item> stackOfMax;

    public StackWithMaximum() {
        this.stack = new Stack<>();
        this.stackOfMax = new Stack<>();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    /**
     * Returns the number of items in the stack.
     *
     * @return the number of items in the stack
     */
    public int size() {
        return stack.size();
    }

    /**
     * Adds the item to this stack.
     *
     * @param item the item to add
     */
    public void push(Item item) {
        if (!(item instanceof Comparable)) {
            throw new IllegalArgumentException("item must be Comparable");
        }
        if (stackOfMax.isEmpty()) {
            stackOfMax.push(item);
        } else {
            if (((Comparable) item).compareTo(stackOfMax.peek()) >= 0) {
                stackOfMax.push(item);
            }
        }
        stack.push(item);
    }

    /**
     * Removes and returns the item most recently added to this stack.
     *
     * @return the item most recently added
     * @throws java.util.NoSuchElementException if this stack is empty
     */
    public Item pop() {
        Item item = stack.pop();
        if (((Comparable) item).compareTo(stackOfMax.peek()) == 0) {
            stackOfMax.pop();
        }
        return item;
    }

    /**
     * Returns (but does not remove) the item most recently added to this stack.
     *
     * @return the item most recently added to this stack
     * @throws java.util.NoSuchElementException if this stack is empty
     */
    public Item peek() {
        return stack.peek();
    }

    public Item max() {
        return stackOfMax.peek();
    }

    /**
     * Returns a string representation of this stack.
     *
     * @return the sequence of items in the stack in LIFO order, separated by
     * spaces
     */
    public String toString() {
        return stack.toString();
    }

    /**
     * Returns an iterator to this stack that iterates through the items in LIFO
     * order.
     *
     * @return an iterator to this stack that iterates through the items in LIFO
     * order.
     */
    public Iterator<Item> iterator() {
        return stack.iterator();
    }

    public static void main(String[] args) {
        StackWithMaximum<Integer> integerStack = new StackWithMaximum<>();
        int i = 1;
        for (; i < 10; i++) {
            integerStack.push(i);
            if(i%6==0){
                integerStack.push(100);
            }
            integerStack.push(i * 3);
        }
        integerStack.push(27);
        integerStack.push(27);
        integerStack.push(27);
        while (!integerStack.isEmpty()) {
            System.out.println(integerStack.max());
            System.out.println(integerStack.pop());

        }

    }

}
