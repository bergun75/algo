/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bergun
 */
public class FindCommonItemsStats {

    public static void main(String[] args) {

        for (int arraysize = 128; arraysize < 1000000; arraysize = arraysize * 2) {
            Integer[] a = new Integer[arraysize];
            Integer[] b = new Integer[arraysize];
            Integer[] c = new Integer[arraysize];
            for (int z = 0; z < arraysize; z++) {
                a[z] = StdRandom.uniform(arraysize * 10);
                b[z] = StdRandom.uniform(arraysize * 10);
                c[z] = StdRandom.uniform(arraysize * 10);
            }
            FindCommonItems commons = new FindCommonItems();
            Stopwatch stopwatch = new Stopwatch();
            SET<Integer> commonItems = commons.findCommonItems(a, b, c);
            double seconds = stopwatch.elapsedTime();
            System.out.printf("%-25d : %10.5f : foundItems:%d\n", arraysize, seconds, commonItems.size());
        }

    }

}
