
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bergun
 */
public class FindCommonItems {

    public static final int MAX_INTEGER = 100000;

    public static final int MIN_INTEGER = 0;

    public static void main(String[] args) {
        int arraySize = 100;
        Integer[] a = new Integer[arraySize];
        Integer[] b = new Integer[arraySize];
        Integer[] c = new Integer[arraySize];

        for (int i = 0; i < arraySize; i++) {
            a[i] = StdRandom.uniform(MIN_INTEGER, MAX_INTEGER);
            b[i] = StdRandom.uniform(MIN_INTEGER, MAX_INTEGER);
            c[i] = StdRandom.uniform(MIN_INTEGER, MAX_INTEGER);
        }

        FindCommonItems commons = new FindCommonItems();
        SET<Integer> commonInts = commons.findCommonItems(a, b, c);
        for (Integer commonInt : commonInts) {
            System.out.printf("%d is a common int\n", commonInt);

        }

    }

    public SET<Integer> findCommonItems(Integer[] a, Integer[] b, Integer[] c) {
        SET<Integer> commonItems = new SET<Integer>();
        Merge.sort(a);
        Merge.sort(b);
        Merge.sort(c); // 3*N(log2N)

        for (int found : a) {//N

            if (Arrays.binarySearch(b, found) > 0) {
                commonItems.add(found); //log2N
            }
            if (Arrays.binarySearch(c, found) > 0) {
                commonItems.add(found);
            }
            //N*2*log2N           
        }
        for (int found : b) {
            if (Arrays.binarySearch(c, found) > 0) {
                commonItems.add(found);
            }
            // N*lognN
        }
        
        return commonItems;

    }

}


