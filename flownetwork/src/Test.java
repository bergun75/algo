import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FordFulkerson;
import java.util.*;

public class Test
  {
  public static void main (String ... args)
    {
    FlowNetwork fn = createSampleNetwork3();
    FordFulkerson ff = new FordFulkerson(fn, 0, fn.V()-1);
    System.out.println(fn);
    Set<Integer> results = new TreeSet<>();
    for (FlowEdge edge:fn.adj(0))
      {
      if(edge.capacity() == edge.flow())
        {
        results.add(edge.to());
        }
      }
    for(int res:results)
      {
      System.out.printf("%d,",res);
      }
    System.out.println();
    }
  public static FlowNetwork createSampleNetwork()
    {
    FlowNetwork fn = new FlowNetwork(8);
    fn.addEdge(new FlowEdge(0, 1, 8100));
    fn.addEdge(new FlowEdge(1, 2, 465));
    fn.addEdge(new FlowEdge(1, 3, 1395));
    fn.addEdge(new FlowEdge(1, 4, 6978));
    fn.addEdge(new FlowEdge(1, 5, 7443));
    fn.addEdge(new FlowEdge(1, 6, 11785));
    fn.addEdge(new FlowEdge(2, 7, 7848));
    fn.addEdge(new FlowEdge(3, 7, 3018));
    fn.addEdge(new FlowEdge(4, 7, 5338));
    fn.addEdge(new FlowEdge(5, 7, 4634));
    fn.addEdge(new FlowEdge(6, 7, 7230));
    return fn;
    }
  public static FlowNetwork createSampleNetwork3()
    {
    FlowNetwork fn = new FlowNetwork(6);
    fn.addEdge(new FlowEdge(4, 5, 7500));
    fn.addEdge(new FlowEdge(0, 1, 6369));
    fn.addEdge(new FlowEdge(0, 2, 1455));
    fn.addEdge(new FlowEdge(0, 3, 6002));
    fn.addEdge(new FlowEdge(1, 4, 6369));
    fn.addEdge(new FlowEdge(2, 4, 1455));
    fn.addEdge(new FlowEdge(3, 4, 6002));
    return fn;
    }
  public static FlowNetwork createSampleNetwork2()
    {
    FlowNetwork fn = new FlowNetwork(7);
    double totalp = 74 + 16 + 52;
    double totalw = 14.55 + 3.98 + 63.69;
    double medianw = totalw/3.0;
    fn.addEdge(new FlowEdge(0, 1, Double.POSITIVE_INFINITY));
    double ratio1 = ((74.0 / totalp)*totalw) - medianw;
    double ratio2 = ((16.0 / totalp)*totalw) - medianw;
    double ratio3 = ((52.0 / totalp)*totalw) - medianw;
    System.out.printf ("%8.6f %8.6f %8.6f", ratio1, ratio2, ratio3);
    fn.addEdge(new FlowEdge(1, 2, 14.55 + ratio1));
    fn.addEdge(new FlowEdge(1, 3, 0));
    fn.addEdge(new FlowEdge(1, 4, 63.69 + ratio3));
    fn.addEdge(new FlowEdge(2, 5, 14.55));
    fn.addEdge(new FlowEdge(3, 5, 3.98));
    fn.addEdge(new FlowEdge(4, 5, 63.69));
    fn.addEdge(new FlowEdge(5, 6, 75));
    return fn;
    }
  public static FlowNetwork createSampleNetwork4()
    {
    FlowNetwork fn = new FlowNetwork(8);
    fn.addEdge(new FlowEdge(6, 7, 2.0));
    fn.addEdge(new FlowEdge(0, 1, 1.0));
    fn.addEdge(new FlowEdge(0, 2, 1.0));
    fn.addEdge(new FlowEdge(0, 3, 2.0));
    fn.addEdge(new FlowEdge(0, 4, 1.0));
    fn.addEdge(new FlowEdge(0, 5, 0.0));
    fn.addEdge(new FlowEdge(1, 6, Double.POSITIVE_INFINITY));
    fn.addEdge(new FlowEdge(2, 6, Double.POSITIVE_INFINITY));
    fn.addEdge(new FlowEdge(3, 6, Double.POSITIVE_INFINITY));
    fn.addEdge(new FlowEdge(4, 6, Double.POSITIVE_INFINITY));
    fn.addEdge(new FlowEdge(5, 6, Double.POSITIVE_INFINITY));
    return fn;
    }
  }
