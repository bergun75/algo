import java.lang.*;

public class Parantheses{
    public static void main(String...args){
        // TODO args check
        System.out.println(validateParantheses(args[0]));
        
    }
    private static boolean validateParantheses(String arg){
        Stack<Character> paranStack = new Stack<>();
        try{
            for (int i = 0; i < arg.length();i++){
                // TODO non paranthesis stuff check
                Character inChar = arg.charAt(i);
                if(inChar==')'){
                    validateChar(paranStack,'(');
                }else if (inChar=='}'){                
                    validateChar(paranStack,'{');
                }else if (inChar==']'){
                    validateChar(paranStack,'[');
                }else{
                    paranStack.push(inChar);
                }
            }
        } catch(IllegalArgumentException e){
            return false;
        }
        if(paranStack.size()!=0){
            return false;
        }
        return true;
    }

    // TODO might be better named
    private static void validateChar(Stack <Character> paranStack, char expectedChar){
        if(paranStack.size()==0){
            throw new IllegalArgumentException();
        }   
        char poppedOut = paranStack.pop();
        if(poppedOut!=expectedChar){
            throw new IllegalArgumentException();
        }
    }
}
