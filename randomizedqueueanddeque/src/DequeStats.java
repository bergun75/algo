/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bergun
 */
public class DequeStats {

    public static void main(String... args) {
        for (int arraysize = 128; arraysize < 10000000; arraysize = arraysize * 2) {
            Deque<Integer> deque = new Deque<>();
            Stopwatch stopwatch = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                deque.addFirst(z);
            }
            double seconds = stopwatch.elapsedTime();
            System.out.printf("%-25d : %10.5f : addFirst:%d\n", arraysize, seconds, deque.size());

            Stopwatch stopwatch2 = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                deque.removeLast();
            }
            double seconds2 = stopwatch2.elapsedTime();
            System.out.printf("%-25d : %10.5f : removeLast deque.size:%d\n", arraysize, seconds2, deque.size());

            Stopwatch stopwatch3 = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                deque.addLast(z);
            }
            double seconds3 = stopwatch3.elapsedTime();
            System.out.printf("%-25d : %10.5f : addLast deque.size:%d\n", arraysize, seconds3, deque.size());

            Stopwatch stopwatch4 = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                deque.removeFirst();
            }
            double seconds4 = stopwatch4.elapsedTime();
            System.out.printf("%-25d : %10.5f : removeFirst deque.size:%d\n", arraysize, seconds4, deque.size());

        }
    }

}
