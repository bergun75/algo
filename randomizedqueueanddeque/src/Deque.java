
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author bergun
 */
public class Deque<Item> implements Iterable<Item> 
  {
  private int N;               // number of elements on queue
  private Node<Item> first;    // beginning of queue
  private Node<Item> last;     // end of queue

  private static class Node<Item> 
    {
    private Item item;
    private Node<Item> next;
    private Node<Item> previous;
    }

  public Deque() 
    {
    first = null;
    last = null;
    N = 0;
    }

  // is the deque empty?
  public boolean isEmpty() 
    {
    return N == 0;
    }

  // return the number of items on the deque
  public int size() 
    {
    return N;
    }

  // add the item to the front
  public void addFirst(Item item) 
    {
    validateItem(item);
    Node<Item> oldFirst = first;
    first = new Node<>();
    first.item = item;
    first.next = oldFirst;
    if (null != oldFirst) 
      {
      oldFirst.previous = first;
      }
    first.previous = null;
    if (isEmpty()) // TODO might not be needed
      {
      last = first;
      }
    N++;
    }

  // add the item to the end
  public void addLast(Item item) 
    {
    validateItem(item);
    Node<Item> oldLast = last;
    last = new Node<>();
    last.item = item;
    last.next = null;
    last.previous = oldLast;
    if (null != oldLast) 
      {
      oldLast.next = last;
      }
    if (isEmpty()) // TODO might not be needed?
      {
      first = last;
      }
    N++;
    }

  private void validateItem(Item item) 
    {
    if (null == item) 
      {
      throw new NullPointerException("item cannot be null");
      }
    }

  // remove and return the item from the front
  public Item removeFirst() 
    {
    validateRemove();
    Item oldItem = first.item;
    first = first.next;
    if (null != first) 
      {
      first.previous = null;
      }
    N--;
    if (isEmpty()) 
      {
      last = null;
      }
    return oldItem;
    }

  // remove and return the item from the end
  public Item removeLast() 
    {
    validateRemove();
    Item oldLast = last.item;
    last = last.previous;
    if (null != last) 
      {
      last.next = null;
      }
    N--;
    if (isEmpty()) 
      {
      first = null;
      }
    return oldLast;
    }

  private void validateRemove() 
    {
    if (isEmpty()) 
      {
      throw new NoSuchElementException();
      }
    }

  // return an iterator over items in order from front to end
  public Iterator<Item> iterator() 
    {
    return new ListIterator<>(first);
    }

  // unit testing
  public static void main(String[] args) 
    {
    Deque<Integer> intDeque = new Deque<>();
    intDeque.addFirst(2);
    intDeque.addLast(3);
    intDeque.addFirst(1);
    intDeque.addLast(4);
    for (int found : intDeque) 
      {
      System.out.println(found);
      }
    System.out.println(intDeque.removeFirst());
    System.out.println(intDeque.removeLast());
    System.out.println(intDeque.removeFirst());
    System.out.println(intDeque.removeLast());
    }

  // an iterator, doesn't implement remove() since it's optional
  private class ListIterator<Item> implements Iterator<Item> 
    {
    private Node<Item> current;
    public ListIterator(Node<Item> first) 
      {
      current = first;
      }
    @Override
    public boolean hasNext() 
      {
      return current != null;
      }
    @Override
    public Item next() 
      {
      if (!hasNext()) 
        {
        throw new NoSuchElementException();
        }
      Item item = current.item;
      current = current.next;
      return item;
      }

    @Override
    public void remove() 
      {
      throw new UnsupportedOperationException();
      }
    }
  }
