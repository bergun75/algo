import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdRandom;


/**
 *
 * @author bergun
 */

public class RandomizedQueue<Item> implements Iterable<Item> 
  {
  private int N;
  private int headIndex, tailIndex;
  private Object[] items;

  public RandomizedQueue() 
    {
    final Object[] a = new Object[10];
    items = a;
    }

  public boolean isEmpty() 
    { // is the queue empty?
    return N == 0;
    }

  public int size() 
    { // return the number of items on the queue
    return N;
    }

  public void enqueue(Item item) 
    { // add the item
    if (null == item) 
      {
      throw new NullPointerException();
      }
    items[tailIndex++] = item;
    N++;
    if (tailIndex >= items.length) 
      {
      increaseArraySize();
      }
    }

  private void increaseArraySize() 
    {
    final Object[] a = new Object[items.length * 2];
    copyArray(items, a);
    }

  @SuppressWarnings("unchecked")
  public Item dequeue() 
    { // remove and return a random item
    validateDequeOrSample();
    int randomIndex = StdRandom.uniform(headIndex, tailIndex);
    Object toDequeue = items[randomIndex];
    if (randomIndex != headIndex) 
      {
      items[randomIndex] = items[headIndex];
      }
    items[headIndex++] = null;
    N--;
    if (N > 0 && N < (items.length >> 2)) 
      {
      decreaseArraySize();
      }
    return (Item) toDequeue;
    }

  private void validateDequeOrSample() 
    {
    if (isEmpty()) 
      {
      throw new NoSuchElementException("Queue underflow");
      }
    }

  private void decreaseArraySize() 
    {
    final Object[] a = new Object[items.length / 2];
    copyArray(items, a);
    }

  private void copyArray(Object[] src, Object[] dst) 
    {
    int count = 0;
    for (int i = headIndex; i < tailIndex; i++) 
      {
      dst[count++] = src[i];
      }
    items = dst;
    headIndex = 0;
    tailIndex = N;
    }

  @SuppressWarnings("unchecked")
  public Item sample() 
    {
    validateDequeOrSample();
    int randomIndex = StdRandom.uniform(headIndex, tailIndex);
    return (Item) items[randomIndex];
    }

  @SuppressWarnings("unchecked")
  public Iterator<Item> iterator() 
    { // return an independent iterator over
      // items in random order
    ListIterator<Item> listIterator = new ListIterator<>(items, tailIndex, headIndex);
    return listIterator;
    }

  public static void main(String[] args) 
    {
    RandomizedQueue<Integer> rQueue = new RandomizedQueue<>();
    for (int i = 0; i < 100; i++) 
      {
      rQueue.enqueue(i);
      }
    int[] array = new int[rQueue.size()];
    int count = 0;
    for (int found : rQueue)    
      {
      array[count++] = found;
      }
    for (int found : array) 
      {
      System.out.println(found);
      }
    int[] array2 = new int[rQueue.size()];
    count = 0;
    while (!rQueue.isEmpty()) 
      {
      array2[count++] = rQueue.dequeue();
      }
    for (int found : array2) 
      {
      System.out.println(found);
      }
    }

  private class ListIterator<Item> implements Iterator<Item> 
    {
    private final Object[] items;
    private int iterHeadIndex;

    public ListIterator(Object[] items, int tailIndex, int headIndex) 
      {
      final Object[] a = new Object[tailIndex - headIndex];
      int indexHead = headIndex;
      for (int i = 0; i < a.length; i++) 
        {
        a[i] = items[indexHead++];
        }
      this.items = a;
      }

    @Override
    public boolean hasNext() 
      {
      return iterHeadIndex < items.length;
      }

    @Override
    @SuppressWarnings("unchecked")
    public Item next() 
      {
      if (!hasNext()) 
        {
        throw new NoSuchElementException();
        }
      int randomIndex = StdRandom.uniform(iterHeadIndex, items.length);
      Item item = (Item) items[randomIndex];
      if (iterHeadIndex != randomIndex) 
        {
        items[randomIndex] = items[iterHeadIndex];
        }
      iterHeadIndex++;
      return item;
      }

    @Override
    public void remove() 
      {
      throw new UnsupportedOperationException();
      }
    }
  }
