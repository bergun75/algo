/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bergun
 */
public class RandomizedQueueStats {

    public static void main(String... args) {
        for (int arraysize = 128; arraysize < 100000000; arraysize = arraysize * 2) {
            RandomizedQueue<Integer> randomizedQueue = new RandomizedQueue<>();
            Stopwatch stopwatch = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                randomizedQueue.enqueue(z);
            }
            double seconds = stopwatch.elapsedTime();
            System.out.printf("%-25d : %10.5f : enqueuedItems:%d\n", arraysize, seconds, randomizedQueue.size());
            
            Stopwatch stopwatch2 = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                randomizedQueue.dequeue();
            }
            double seconds2 = stopwatch2.elapsedTime();
            System.out.printf("%-25d : %10.5f : dequeuedItems:%d\n", arraysize, seconds2, randomizedQueue.size());

        }
    }

}
