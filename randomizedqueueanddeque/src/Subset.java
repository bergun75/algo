import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdRandom;

/**
 *
 * @author bergun
 */
public class Subset {

    public static void main(String[] args) {
        if (args.length != 1) 
          {
          throw new IllegalArgumentException("must provide an Integer argument");
          }
        int value = Integer.parseInt(args[0]);
        RandomizedQueue<String> queue = new RandomizedQueue<>();
        String[] allStrings = StdIn.readAllStrings();
        if (value < 0 || value > allStrings.length) 
          {
          throw new IllegalArgumentException("0 <= k <= N");
          }
        for (int i = 0; i < allStrings.length; i++) 
          {
          if (queue.size() == value) 
            {
            break;
            }
          int rndIndex = StdRandom.uniform(i, allStrings.length);
          queue.enqueue(allStrings[rndIndex]);
          allStrings[rndIndex] = allStrings[i];            
          }
        while (!queue.isEmpty()) 
          {
          System.out.println(queue.dequeue());
          }
    }
}
