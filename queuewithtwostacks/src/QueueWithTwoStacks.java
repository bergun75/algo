
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bergun
 */
public class QueueWithTwoStacks<Item> implements Iterable<Item> {

    private final Stack<Item> enqueuStack;
    private final Stack<Item> dequeStack;

    /**
     * Initializes an empty queue.
     */
    public QueueWithTwoStacks() {
        enqueuStack = new Stack<>();
        dequeStack = new Stack<>();
    }

    /**
     * Is this queue empty?
     *
     * @return true if this queue is empty; false otherwise
     */
    public boolean isEmpty() {
        return enqueuStack.isEmpty() && dequeStack.isEmpty();
    }

    /**
     * Returns the number of items in this queue.
     *
     * @return the number of items in this queue
     */
    public int size() {
        return enqueuStack.size() + dequeStack.size();
    }

    /**
     * Returns the item least recently added to this queue.
     *
     * @return the item least recently added to this queue
     * @throws java.util.NoSuchElementException if this queue is empty
     */
    public Item peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }
        transferAllToDequeStack();
        return dequeStack.peek();
    }

    /**
     * Adds the item to this queue.
     *
     * @param item the item to add
     */
    public void enqueue(Item item) {
        enqueuStack.push(item);
    }

    /**
     * Removes and returns the item on this queue that was least recently added.
     *
     * @return the item on this queue that was least recently added
     * @throws java.util.NoSuchElementException if this queue is empty
     */
    public Item dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue underflow");
        }
        transferAllToDequeStack();
        return dequeStack.pop();
    }

    /**
     * Returns a string representation of this queue.
     *
     * @return the sequence of items in FIFO order, separated by spaces
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Item item : this) {
            s.append(item + " ");
        }
        return s.toString();
    }

    /**
     * Returns an iterator that iterates over the items in this queue in FIFO
     * order.
     *
     * @return an iterator that iterates over the items in this queue in FIFO
     * order
     */
    public Iterator<Item> iterator() {
        transferAllToDequeStack();
        return dequeStack.iterator();
    }

    /**
     * Every time the deque or peek requested or an iterator is started the
     * enqueuStack is drained to deque stack so that FIFO is provided. time
     * complexity would be linear when this is done but after one time this is
     * done other peek operations wouldnt have to do the same procedure. This is
     * the same as resizing array when needed
     *
     */
    private void transferAllToDequeStack() {
        while (!enqueuStack.isEmpty()) {
            dequeStack.push(enqueuStack.pop());
        }
    }

    /**
     * Unit tests the <tt>QueueWithTwoStacks</tt> data type.
     */
    public static void main(String[] args) {
        QueueWithTwoStacks<Integer> queue = new QueueWithTwoStacks<>();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        for(Integer found:queue){
            System.out.println(found);
        }
        while (!queue.isEmpty()) {
            System.out.println(queue.dequeue());
        }
    }

}
