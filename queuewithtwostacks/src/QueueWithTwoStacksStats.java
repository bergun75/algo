
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author bergun
 */
public class QueueWithTwoStacksStats {

    /**
     * Unit tests the <tt>QueueWithTwoStacks</tt> data type.
     */
    public static void main(String[] args) {
        for (int arraysize = 128; arraysize < 10000000; arraysize = arraysize * 2) {
            QueueWithTwoStacks<Integer> queue = new QueueWithTwoStacks<>();
            Stopwatch stopwatch = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                queue.enqueue(z);
            }
            double seconds = stopwatch.elapsedTime();
            System.out.printf("%-25d : %10.5f : enqueue size: %d\n", arraysize, seconds, queue.size());

            Stopwatch stopwatch2 = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                queue.dequeue();
            }
            double seconds2 = stopwatch2.elapsedTime();
            System.out.printf("%-25d : %10.5f : deque size:%d\n", arraysize, seconds2, queue.size());

            Stopwatch stopwatch3 = new Stopwatch();
            for (int z = 0; z < arraysize; z++) {
                queue.enqueue(z);
                if (z % 5 == 0) {
                    queue.dequeue();
                }
            }
            while (!queue.isEmpty()) {
                queue.dequeue();
            }
            double seconds3 = stopwatch3.elapsedTime();
            System.out.printf("%-25d : %10.5f : enque-deque size:%d\n", arraysize, seconds3, queue.size());

        }

    }

}
