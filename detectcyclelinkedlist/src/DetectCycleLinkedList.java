
/**
 *
 * @author bergun
 */
public class DetectCycleLinkedList {

    /**
     * @param args the command line arguments
     */
    private static class Node<Item> {

        private Item item;
        private Node<Item> next;
    }

    public static void main(String[] args) {
        Node<Integer> current = new Node<>();
        Node<Integer> cycle = current;
        current.item = 0;
        for (int i = 1; i < 100; i++) {
            Node<Integer> nextNode = new Node<>();
            nextNode.item = i;
            if (i % 59  == 0) {
                current.next = cycle;
            } else {
                current.next = nextNode;

            }
            current = nextNode;

        }
        current.next = null;

        System.out.println(findCyclicNode(cycle));

    }

    public static <Item> Item findCyclicNode(Node<Item> start) {
        Node<Item> tortoise, hare;
        tortoise = hare = start;
        while (tortoise.next != null && hare.next !=null && hare.next.next !=null) {
            Node<Item> tmp1 = tortoise.next;
            Node<Item> tmp2 = hare.next.next;
            if (tmp1 == tmp2) {
                return tortoise.item;
            } else {
                tortoise = tmp1;
                hare = tmp2;
            }
        }
        return null;
    }

}
