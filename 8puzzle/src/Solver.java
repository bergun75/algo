import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.In;

/**
 *
 * @author bergun
 */
public class Solver 
  {
  private static class Node<Item> 
    {
    private Item item;
    private Node<Item> previous;
    private final List<Node<Item>> children = new ArrayList<>();
    private int height;
    }

  private static final BiFunction<Node<Board>, Node<Board>, Integer> BI_COMP =
    (Node<Board> o1, Node<Board> o2)->
    {
    int diff = (o1.item.manhattan() + o1.height) -
               (o2.item.manhattan() + o2.height);
    if (diff == 0)
      {
      return o2.height - o1.height;
      }
    return diff;
    };
  
  private final Stack<Board> solutionBoard;

  public Solver(Board initial) 
    {
    if (null == initial) 
      {
      throw new NullPointerException();
      }
    this.solutionBoard = solveInternal(initial);
    }

  private Stack<Board> solveInternal(Board board) 
    {
    MinPQ<Node<Board>> pQueueReal = new MinPQ<>(BI_COMP::apply);
    MinPQ<Node<Board>> pQueueTwin = new MinPQ<>(BI_COMP::apply);
    Node<Board> realNodePointer = nodifyBoard(board, null, 0);
    Node<Board> twinNodePointer = nodifyBoard(board.twin(), null, 0);
    while (!realNodePointer.item.isGoal())
      {
      realNodePointer = chooseNextBoard(pQueueReal, realNodePointer);
      twinNodePointer = chooseNextBoard(pQueueTwin, twinNodePointer);
      if (twinNodePointer.item.isGoal())
        {
        return null;
        }
      }
    return solveSearchNode(realNodePointer);
    }

  private Stack<Board> solveSearchNode(Node<Board> searchNodePointer)
    {
    Stack<Board> nodeStack = new Stack<>();
    Node<Board> counterPointer = searchNodePointer;
    while (counterPointer.previous != null) 
      {
      nodeStack.push(counterPointer.item);
      counterPointer = counterPointer.previous;
      }
    nodeStack.push(counterPointer.item);
    return nodeStack;
    }

  private Node<Board> chooseNextBoard(MinPQ<Node<Board>> pQueue,
                                      Node<Board> nodePointer)
    {
    for (Board neigbor : nodePointer.item.neighbors()) 
      {
      Node<Board> neig = nodifyBoard(neigbor, 
                                     nodePointer, 
                                     nodePointer.height + 1);
      nodePointer.children.add(neig);
      if (null == nodePointer.previous
              || !neigbor.equals(nodePointer.previous.item)) 
        {
        pQueue.insert(neig);
        }
      }
    return pQueue.delMin();
    }

  private Node<Board> nodifyBoard(Board board,
                                  Node<Board> previous,
                                  int height)
    {
    Node<Board> nodePointer = new Node<>();
    nodePointer.item = board;
    nodePointer.previous = previous;
    nodePointer.height = height;
    return nodePointer;
    }

  // is the initial board solvable?
  public boolean isSolvable() 
    {
    return this.solutionBoard != null;
    }

  // min number of moves to solve initial board; -1 if unsolvable
  public int moves() 
    {
    if (solutionBoard == null)
      {
    return -1;
      }
    return solutionBoard.size() - 1;
    }

  // sequence of boards in a shortest solution; null if unsolvable
  public Iterable<Board> solution() 
    {
    return solutionBoard;
    }


  // solve a slider puzzle (given below)
  public static void main(String[] args) 
    {
      for (String file : args) 
        {
        int[][] puzzle = readBoardFromFile(file);
        Board initial = new Board(puzzle);

        // solve the puzzle
        Solver solver = new Solver(initial);
          // print solution to standard output
        if (!solver.isSolvable()) 
          {
          StdOut.println("No solution possible");
          } 
        else 
          {
          StdOut.println("Minimum number of moves = " + solver.moves());
          for (Board board : solver.solution()) 
            {
            StdOut.println(board);
            }
          }
        }
    }

  private static int[][] readBoardFromFile(String fileName) 
    {
    In scanner = new In(fileName);
    int[] members = scanner.readAllInts();
    int puzzleSize = members[0];
    int[][] puzzle = new int[puzzleSize][puzzleSize];
    for (int i = 1; i < members.length; i++) 
      {
      int row = (i - 1) / puzzleSize;
      int column = i - 1 - (row * puzzleSize);
      puzzle[row][column] = members[i];
      }
    return puzzle;
    }

}
