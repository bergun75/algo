import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.In;

/**
 *
 * @author bergun
 */
public class Board 
  {
  private final int[] board;
  private final int edgeLength;
  private int blankBlockPosition;

  private enum BoardType 
    {
    TWIN(0), UP(1), DOWN(2), RIGHT(3), LEFT(4), COPY(5);
    private int val;
    BoardType(int val) 
      {
      this.val = val;
      }

    public int getVal() 
      {
      return val;
      }
    }

  public Board(int[][] blocks) // construct a board from an N-by-N array of blocks
    { // (where blocks[i][j] = block in row i, column j)        
    board = new int[blocks.length*blocks.length];
    edgeLength = blocks.length;
    for (int row = 0; row < blocks.length; row++) 
      {
      if (blocks[row].length != blocks.length) 
        {
        throw new IllegalArgumentException();
        }
      for (int col = 0; col < blocks[row].length; col++) 
        {
        int idx = col+(row*blocks.length);
        if (blocks[row][col] == 0) 
          {
          blankBlockPosition = idx;
          }
        board[idx] = blocks[row][col];
        }
      }
    }

  private Board(int[] board, 
                BoardType type,
                int nullBlockPos,
                int edgeLength)
    {
    this.board = new int[board.length];
    this.edgeLength = edgeLength;
    for (int idx = 0; idx < board.length; idx++) 
      {
      this.board[idx] = board[idx];
      }
    boolean moveNullBlock = false;
    switch (type) 
      {
      case UP:
        if (nullBlockPos / edgeLength  == 0) 
          {
          throw new NoSuchElementException();
          }
        blankBlockPosition = nullBlockPos - edgeLength;
        moveNullBlock = true;
        break;
      case LEFT:
        if (nullBlockPos % edgeLength == 0) 
          {
          throw new NoSuchElementException();
          }
        blankBlockPosition = nullBlockPos - 1;
        moveNullBlock = true;
          break;
      case DOWN:
        if (nullBlockPos / edgeLength == (edgeLength - 1)) 
          {
          throw new NoSuchElementException();
          }
        blankBlockPosition = nullBlockPos + edgeLength;
        moveNullBlock = true;
          break;
      case RIGHT:
        if (nullBlockPos % edgeLength == (edgeLength - 1)) 
          {
          throw new NoSuchElementException();
          }
        blankBlockPosition = nullBlockPos + 1;
        moveNullBlock = true;
          break;
      case TWIN:
        blankBlockPosition = nullBlockPos;
        int nonBlockLine = ((blankBlockPosition / edgeLength) + 1) 
                            % edgeLength;
        int targetBlockSwap = 0 + (nonBlockLine*edgeLength);
        int temp = this.board[targetBlockSwap];
        this.board[targetBlockSwap] = this.board[targetBlockSwap+1];
        this.board[targetBlockSwap+1] = temp;
        break;
      default:
        throw new NoSuchElementException();

      }
    if (moveNullBlock) 
      {
      int temp = this.board[blankBlockPosition];
      this.board[blankBlockPosition] = 0;
      this.board[nullBlockPos] = temp;
      } 
    }

  public int dimension() 
    {
    return edgeLength;
    }

  public int hamming() 
    {
    int totalDistance = 0;
    for (int idx = 0; idx < board.length; idx++) 
      {
      if (isInItsPlace(idx)) 
        {
        totalDistance++;
        }
      }
    return totalDistance;
    }

  private boolean isInItsPlace(int index) 
    {
    int expectedValue = index + 1;
    if (board[index] == 0)
      {
      return false;
      }
    return board[index] != expectedValue;
    }

  // sum of Manhattan distances between blocks and goal
  public int manhattan() 
    {
    int totalDistance = 0;
    for (int idx = 0; idx < board.length; idx++) 
      {
      if (board[idx] == 0) 
        {
        continue;
        }
      totalDistance += calculateManhattanDist(idx);
      }
    return totalDistance;
    }

  private int calculateManhattanDist(int idx) 
    {
    int expectedVal = board[idx] - 1;
    int expectedRow = expectedVal / edgeLength;
    int expectedCol = expectedVal % edgeLength;
    int existingRow = idx / edgeLength;
    int existingCol = idx % edgeLength;
    int diffCol = Math.abs(expectedCol - existingCol);
    int diffRow = Math.abs(expectedRow - existingRow);
    return diffCol + diffRow;
    }

  // is this board the goal board?
  public boolean isGoal() 
    {
    int checkZero = manhattan();
    return checkZero == 0;
    }

  // a boadr that is obtained by exchanging two adjacent blocks in the same row
  public Board twin() 
    {
    return new Board(this.board, 
                     BoardType.TWIN, 
                     this.blankBlockPosition,
                     this.edgeLength);
    }
  
  // does this board equal y?
  @Override
  public boolean equals(Object y) 
    {
      if (null == y) 
        {
        return false;
        }
      if (getClass() != y.getClass()) 
        {
        return false;
        }
      Board other = (Board) y;
      if (other.dimension() != this.dimension()) 
        {
        return false;
        }
      for (int idx = 0; idx < board.length; idx++) 
        {
        if (board[idx] != other.board[idx]) 
          {
          return false;
          }
        }
      return true;
    }

  // all neighboring boards
  public Iterable<Board> neighbors() 
    {
    Queue<Board> boardQ = new Queue<>();
    for (BoardType val : BoardType.values()) 
      {
      if (val != BoardType.TWIN) 
        {
        try 
          {
            Board neigBor = new Board(this.board, 
                                      val, 
                                      blankBlockPosition,
                                      this.edgeLength);
            boardQ.enqueue(neigBor);
          } 
        catch (NoSuchElementException e) 
          {
              // Nothing
          }
        }
      }
    return boardQ;
    }

  @Override
  public String toString() // string representation of this board
    {
    int digitSize = Integer.toString(edgeLength * edgeLength).length();
    StringBuilder builder = new StringBuilder(edgeLength + "\n");
    for (int idx = 0; idx < board.length; idx++) 
      {
      builder.append(
              String.format("%" + digitSize + "d", board[idx]));
      builder.append(" ");
      if (idx % edgeLength == (edgeLength - 1))
        {
        builder.append("\n");
        }
      }
    return builder.toString();

    }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) 
    {
    int[][] puzzle = readBoardFromFile(args[0]);
    Board board = new Board(puzzle);
    System.out.println("board");
    System.out.println(board);
    System.out.println(board.hamming());
    Board twin = board.twin();
    System.out.println("twin");
    System.out.println(twin);
    }

  private static int[][] readBoardFromFile(String fileName) 
    {
    In scanner = new In(fileName);
    int[] members = scanner.readAllInts();
    int puzzleSize = members[0];
    int[][] puzzle = new int[puzzleSize][puzzleSize];
    for (int i = 1; i < members.length; i++) 
      {
      int row = (i - 1) / puzzleSize;
      int column = i - 1 - (row * puzzleSize);
      puzzle[row][column] = members[i];
      }
    return puzzle;
    }
  }
